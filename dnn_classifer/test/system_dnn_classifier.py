# -*- coding: utf-8 -*-
#import mock
import unittest
import os
import sys
#sys.path.append('../../')
from common import common_utils
import shutil

TEST_STORAGE = '/tmp/storage/unregistered_user/RANDOM_SYSTEM_TEST/'
PATH_TO_DATA = TEST_STORAGE + 'data/'
class TestClass1(unittest.TestCase):
    def setUp(self):
        # copy training file to /tmp/storage/unregistered_user/RANDOM_SYSTEM_TEST/data folder
        os.makedirs(PATH_TO_DATA)
        shutil.copy('../data/train.csv', PATH_TO_DATA)
        shutil.copy('../data/train_empty.csv', PATH_TO_DATA)
        shutil.copy('../data/train_science_data.csv', PATH_TO_DATA)
        shutil.copy('../data/train_tab.csv', PATH_TO_DATA)
        shutil.copy('../data/test.csv', PATH_TO_DATA)
        shutil.copy('../data/test_mismatch.csv', PATH_TO_DATA)
        shutil.copy('../data/test_mismatch_data.csv', PATH_TO_DATA)
        shutil.copy('../data/test_tab.csv', PATH_TO_DATA)
        shutil.copy('../data/predict.csv', PATH_TO_DATA)
        shutil.copy('../data/predict_normal.csv', PATH_TO_DATA)
        shutil.copy('../data/predict_tab.csv', PATH_TO_DATA)
        shutil.copy('../data/train_normal.csv', PATH_TO_DATA)
        return

    def tearDown(self):
        shutil.rmtree(TEST_STORAGE)
        return

    #@unittest.skip('')
    def test_train_test_predict(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        result = result.strip()
        result_list = result.split('\n')
        accuracy = result_list[0].strip()
        self.assertEqual(accuracy, common_utils.LABEL_TEST_ACCURACY+':')
        val = float(result_list[1].strip())
        self.assertEqual(accuracy_range(val), True)

        classification = result_list[2]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':Five')

        classification_result = result_list[3]
        self.assertEqual(classification_result, 'spiecy_2')
        classification_result = result_list[4]
        self.assertEqual(classification_result, 'spiecy_1')
        classification_result = result_list[5]
        self.assertEqual(classification_result, 'spiecy_0')

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

    #@unittest.skip('')
    def test_train_test_predict_empty_train_file(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        train_filename = PATH_TO_DATA + 'train_empty.csv'
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + train_filename)
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        self.assertEqual(error_code, 1)
        self.assertEqual(error_msg.strip(), common_utils.MESSAGE_ERROR_FILE_INVALID_INPUT % train_filename)

    #@unittest.skip('')
    def test_train_only(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        result = result.strip()
        self.assertEqual(len(result), 0)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

    #@unittest.skip('')
    def test_train_with_normalization(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_normal.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        result = result.strip()
        #self.assertEqual(len(result), 0)
        metadata_filename = TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir + '/' + \
                                        common_utils.MODEL_METADATA_FOLDER + common_utils.MODEL_METADATA_FILENAME
        self.assertEqual(os.path.isfile(metadata_filename), True)
        common_utils.BASE_MODEL_FOLDER = TEST_STORAGE + common_utils.BASE_MODEL_FOLDER
        (metadata_dict, err) = common_utils.load_metadata_from_model(model_dir)
        self.assertEqual(metadata_dict[common_utils.METADATA_TAG_NORMALIZATION_LIST], [1.0,1.0,1.0,100.0])

        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict_normal.csv')
        command_list.append(common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(result, 'classification:five\nspiecy_2\nspiecy_1\n')

    #@unittest.skip('')
    def test_train_test(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        result = result.strip()
        result_list = result.split('\n')
        accuracy = result_list[0].strip()
        self.assertEqual(accuracy, common_utils.LABEL_TEST_ACCURACY + ':')
        val = float(result_list[1].strip())
        self.assertEqual(accuracy_range(val), True)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

    #@unittest.skip('')
    def test_train_predict(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        result = result.strip()
        result_list = result.split('\n')
        self.assertEqual(len(result_list), 4)
        classification = result_list[0]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':Five')

        classification_result = result_list[1]
        self.assertEqual(classification_result, 'spiecy_2')
        classification_result = result_list[2]
        self.assertEqual(classification_result, 'spiecy_1')
        classification_result = result_list[3]
        self.assertEqual(classification_result, 'spiecy_0')

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

    #@unittest.skip('')
    def test_load_from_model(self):
        # first train and create model
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        result = result.strip()
        result_list = result.split('\n')
        self.assertEqual(len(result_list), 4)
        classification = result_list[0]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':Five')

        classification_result = result_list[1]
        self.assertEqual(classification_result, 'spiecy_2')
        classification_result = result_list[2]
        self.assertEqual(classification_result, 'spiecy_1')
        classification_result = result_list[3]
        self.assertEqual(classification_result, 'spiecy_0')
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

    #@unittest.skip('')
    def test_load_from_model_different_delimiter(self):
        # first train and create model
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict_tab.csv')
        command_list.append(common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        result = result.strip()
        result_list = result.split('\n')
        self.assertEqual(len(result_list), 4)
        classification = result_list[0]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':Five')

        classification_result = result_list[1]
        self.assertEqual(classification_result, 'spiecy_2')
        classification_result = result_list[2]
        self.assertEqual(classification_result, 'spiecy_1')
        classification_result = result_list[3]
        self.assertEqual(classification_result, 'spiecy_0')
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

    #@unittest.skip('')
    def test_train_scientific(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_science_data.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        result = result.strip()
        self.assertEqual(len(result), 0)
        self.assertEqual(error_code, 0)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

    #@unittest.skip('')
    def test_train_tab(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_tab.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        self.assertEqual(len(result), 0)
        self.assertEqual(error_code, 0)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

    #@unittest.skip('')
    def test_train_test_tab_predict(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test_tab.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        result = result.strip()
        result_list = result.split('\n')
        accuracy = result_list[0].strip()
        self.assertEqual(accuracy, common_utils.LABEL_TEST_ACCURACY + ':')
        val = float(result_list[1].strip())
        self.assertEqual(accuracy_range(val), True)

        classification = result_list[2]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':Five')

        classification_result = result_list[3]
        self.assertEqual(classification_result, 'spiecy_2')
        classification_result = result_list[4]
        self.assertEqual(classification_result, 'spiecy_1')
        classification_result = result_list[5]
        self.assertEqual(classification_result, 'spiecy_0')

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

    #@unittest.skip('')
    def test_train_test_tab_predict_tab(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict_tab.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test_tab.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        result = result.strip()
        result_list = result.split('\n')
        accuracy = result_list[0].strip()
        self.assertEqual(accuracy, common_utils.LABEL_TEST_ACCURACY + ':')
        val = float(result_list[1].strip())
        self.assertEqual(accuracy_range(val), True)

        classification = result_list[2]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':Five')

        classification_result = result_list[3]
        self.assertEqual(classification_result, 'spiecy_2')
        classification_result = result_list[4]
        self.assertEqual(classification_result, 'spiecy_1')
        classification_result = result_list[5]
        self.assertEqual(classification_result, 'spiecy_0')

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)


    #@unittest.skip('')
    def test_train_test_mismatched_field(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test_mismatch.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        self.assertEqual(error_code, 2)
        self.assertEqual(error_msg.strip(), common_utils.MESSAGE_ERROR_MISMATCHED_COLUMNS % ('test_mismatch.csv', 'train.csv'))

        command_list = ['/usr/bin/python', '../dnn_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test_mismatch_data.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(error_code, 1)
        self.assertEqual(error_msg.strip(), \
                         common_utils.MESSAGE_ERROR_MISMATCHED_VALUE_IN_LABEL_COLUMN_WITH_TRAINING_FILE % \
                         ('test_mismatch_data.csv', 'spiecy_456'))


def accuracy_range(val):
    return val > 0.96 and val < 0.97

if __name__ == '__main__':
    unittest.main()
