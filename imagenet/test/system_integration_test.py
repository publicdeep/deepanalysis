# -*- coding: utf-8 -*-
#import mock
import unittest
import os
import sys
sys.path.append('../../')
from common import common_utils
#import shutil

class TestClass1(unittest.TestCase):
    def setUp(self):
        # copy training file to /tmp/storage/unregistered_user/RANDOM_SYSTEM_TEST/data folder
        return

    def tearDown(self):
        return

    #@unittest.skip('')
    def test_image(self):
        command_list = ['/usr/bin/python', '../classify_image.py']
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + '../model')
        command_list.append(common_utils.CMD_OPT_IMGNET_IMAGE_FILE + '=' + 'data/lion.jpg')
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        self.assertEqual(result.strip(), 'lion, king of beasts, Panthera leo\nscore=0.95375')

        return


if __name__ == '__main__':
    unittest.main()
