# -*- coding: utf-8 -*-
#import mock
import unittest
import os
import sys
sys.path.append('../../')
from common import common_utils
import shutil

TEST_STORAGE = '/tmp/storage/unregistered_user/RANDOM_SYSTEM_TEST/'
PATH_TO_DATA = TEST_STORAGE + 'data/'
class TestClass1(unittest.TestCase):
    def setUp(self):
        # copy training file to /tmp/storage/unregistered_user/RANDOM_SYSTEM_TEST/data folder
        os.makedirs(PATH_TO_DATA)
        shutil.copy('../data/train.csv', PATH_TO_DATA)
        shutil.copy('../data/train_science_data.csv', PATH_TO_DATA)
        shutil.copy('../data/train_tab.csv', PATH_TO_DATA)
        shutil.copy('../data/test.csv', PATH_TO_DATA)
        shutil.copy('../data/test_tab.csv', PATH_TO_DATA)
        shutil.copy('../data/predict.csv', PATH_TO_DATA)
        shutil.copy('../data/predict_tab.csv', PATH_TO_DATA)
        return

    def tearDown(self):
        shutil.rmtree(TEST_STORAGE)
        return

    #@unittest.skip('')
    def test_train_test_predict(self):
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        result_list = result.split('\n')
        loss = result_list[0].strip()
        self.assertEqual(loss, common_utils.LABEL_TEST_LOSS+':')

        val = float(result_list[1].strip())
        self.assertEqual(loss_range(val), True)

        prediction = result_list[2].strip()
        self.assertEqual(prediction, common_utils.LABEL_PREDICTION+':'+'SALES')

        val = float(result_list[3].strip())
        self.assertEqual(predict1_range(val), True)

        val = float(result_list[4].strip())
        self.assertEqual(predict2_range(val), True)

        val = float(result_list[5].strip())
        self.assertEqual(predict3_range(val), True)

        val = float(result_list[6].strip())
        self.assertEqual(predict4_range(val), True)

        return

    #@unittest.skip('')
    def test_train_test_predict_with_model(self):
        model_dir='system_model'
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test.csv')
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        result_list = result.split('\n')
        loss = result_list[0].strip()
        self.assertEqual(loss, common_utils.LABEL_TEST_LOSS+':')

        val = float(result_list[1].strip())
        self.assertEqual(loss_range(val), True)

        prediction = result_list[2].strip()
        self.assertEqual(prediction, common_utils.LABEL_PREDICTION+':'+'SALES')

        val = float(result_list[3].strip())
        self.assertEqual(predict1_range(val), True)

        val = float(result_list[4].strip())
        self.assertEqual(predict2_range(val), True)

        val = float(result_list[5].strip())
        self.assertEqual(predict3_range(val), True)

        val = float(result_list[6].strip())
        self.assertEqual(predict4_range(val), True)

        self.verify_and_cleanup_model(model_dir)

        return

    #@unittest.skip('')
    def test_train_test(self):
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        result_list = result.split('\n')
        loss = result_list[0].strip()
        self.assertEqual(loss, common_utils.LABEL_TEST_LOSS+':')

        val = float(result_list[1].strip())
        self.assertEqual(loss_range(val), True)

        return

    #@unittest.skip('')
    def test_train_test_with_model(self):
        model_dir='system_model'
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test.csv')
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        result_list = result.split('\n')
        loss = result_list[0].strip()
        self.assertEqual(loss, common_utils.LABEL_TEST_LOSS+':')

        val = float(result_list[1].strip())
        self.assertEqual(loss_range(val), True)

        self.verify_and_cleanup_model(model_dir)

        return

    #@unittest.skip('')
    def test_train_predict(self):
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)

        result_list = result.split('\n')
        prediction = result_list[0].strip()
        self.assertEqual(prediction, common_utils.LABEL_PREDICTION + ':'+'SALES')

        val = float(result_list[1].strip())
        self.assertEqual(predict1_range(val), True)

        val = float(result_list[2].strip())
        self.assertEqual(predict2_range(val), True)

        val = float(result_list[3].strip())
        self.assertEqual(predict3_range(val), True)

        val = float(result_list[4].strip())
        self.assertEqual(predict4_range(val), True)

        return

    #@unittest.skip('')
    def test_train_predict_with_model(self):
        model_dir='system_model'
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        self.assertEqual(error_code, 0)
        result_list = result.split('\n')
        prediction = result_list[0].strip()
        self.assertEqual(prediction, common_utils.LABEL_PREDICTION + ':'+'SALES')

        val = float(result_list[1].strip())
        self.assertEqual(predict1_range(val), True)

        val = float(result_list[2].strip())
        self.assertEqual(predict2_range(val), True)

        val = float(result_list[3].strip())
        self.assertEqual(predict3_range(val), True)

        val = float(result_list[4].strip())
        self.assertEqual(predict4_range(val), True)

        self.verify_and_cleanup_model(model_dir)

        return

    #@unittest.skip('')
    def test_train_only(self):
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        self.assertEqual(error_code, 0)
        self.assertEqual(len(result), 0)

        return

    #@unittest.skip('')
    def test_train_with_model(self):
        model_dir='system_model'
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        self.assertEqual(error_code, 0)
        self.assertEqual(len(result), 0)

        self.verify_and_cleanup_model(model_dir)

        return

    #@unittest.skip('')
    def test_predict_only(self):
        model_dir='system_model'
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        self.assertEqual(error_code, 0)
        self.assertEqual(len(result), 0)

        #result_model_dir = common_utils.BASE_MODEL_FOLDER + model_dir + '/' + common_utils.MODEL_RESULT_FOLDER
        #command_list = ['/usr/bin/python', '../linear.py', '--predict-filename=../data/predict.csv', common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR + '=' + model_dir]
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        self.assertEqual(error_code, 0)
        result_list = result.split('\n')
        prediction = result_list[0].strip()
        self.assertEqual(prediction, common_utils.LABEL_PREDICTION + ':'+'SALES')

        val = float(result_list[1].strip())
        self.assertEqual(predict1_range(val), True)

        val = float(result_list[2].strip())
        self.assertEqual(predict2_range(val), True)

        val = float(result_list[3].strip())
        self.assertEqual(predict3_range(val), True)

        val = float(result_list[4].strip())
        self.assertEqual(predict4_range(val), True)

        self.verify_and_cleanup_model(model_dir)

        return

    #@unittest.skip('')
    def test_predict_only_with_different_delimiter(self):
        model_dir='system_model'
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        self.assertEqual(error_code, 0)
        self.assertEqual(len(result), 0)

        #result_model_dir = common_utils.BASE_MODEL_FOLDER + model_dir + '/' + common_utils.MODEL_RESULT_FOLDER
        #command_list = ['/usr/bin/python', '../linear.py', '--predict-filename=../data/predict.csv', common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR + '=' + model_dir]
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict_tab.csv')
        command_list.append(common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        self.assertEqual(error_code, 0)
        result_list = result.split('\n')
        prediction = result_list[0].strip()
        self.assertEqual(prediction, common_utils.LABEL_PREDICTION + ':'+'SALES')

        val = float(result_list[1].strip())
        self.assertEqual(predict1_range(val), True)

        val = float(result_list[2].strip())
        self.assertEqual(predict2_range(val), True)

        val = float(result_list[3].strip())
        self.assertEqual(predict3_range(val), True)

        val = float(result_list[4].strip())
        self.assertEqual(predict4_range(val), True)

        self.verify_and_cleanup_model(model_dir)

        return

    #@unittest.skip('')
    def test_train_with_model_scientific_data(self):
        model_dir='system_model'
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_science_data.csv')
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(error_code, 0)
        self.assertEqual(len(result), 0)

        self.verify_and_cleanup_model(model_dir, check_normalization = False)

        return

    #@unittest.skip('')
    def test_tab_train_test_predict_with_model(self):
        model_dir='system_model'
        command_list = ['/usr/bin/python', '../linear.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_tab.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict_tab.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test_tab.csv')
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        result_list = result.split('\n')
        loss = result_list[0].strip()
        self.assertEqual(loss, common_utils.LABEL_TEST_LOSS+':')

        val = float(result_list[1].strip())
        self.assertEqual(loss_range(val), True)

        prediction = result_list[2].strip()
        self.assertEqual(prediction, common_utils.LABEL_PREDICTION+':'+'SALES')

        val = float(result_list[3].strip())
        self.assertEqual(predict1_range(val), True)

        val = float(result_list[4].strip())
        self.assertEqual(predict2_range(val), True)

        val = float(result_list[5].strip())
        self.assertEqual(predict3_range(val), True)

        val = float(result_list[6].strip())
        self.assertEqual(predict4_range(val), True)

        self.verify_and_cleanup_model(model_dir)

        return

    def verify_and_cleanup_model(self, model_dir, check_normalization = True):
        common_utils.OUTPUT_DIR = TEST_STORAGE
        (metadata_dict, err) = common_utils.load_metadata_from_model(model_dir)
        metadata_dict[common_utils.METADATA_TAG_COLUMNS] = ['DAY','FEMALE','MALE','SALES']
        if check_normalization == True:
            metadata_dict[common_utils.METADATA_TAG_NORMALIZATION_LIST] = [1.0,1.0,0.1,1.0]
        else:
            self.assertEqual(common_utils.METADATA_TAG_NORMALIZATION_LIST in metadata_dict, False)

def loss_range(val):
    return val > 14.0 and val < 17.0

def predict1_range(val):
    return val > 19.0 and val < 21.0
def predict2_range(val):
    return val > 21.0 and val < 23.0
def predict3_range(val):
    return val > 24.0 and val < 26.0
def predict4_range(val):
    return val > 26.0 and val < 28.0

if __name__ == '__main__':
    unittest.main()
