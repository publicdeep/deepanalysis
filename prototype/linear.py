# -*- coding: utf-8 -*-
#  Copyright 2016 All Rights Reserved.
#
"""DNNRegressor with custom input_fn.
to run: % python linear.py --train-filename=train.csv --predict-filename=predict.csv
    [--test-filename=test.csv] [--enable-logging] [--model-dir=/tmp/model_dir] [--load-model-dir=/tmp/model_dir]
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pandas as pd
import tensorflow as tf
import sys
import itertools

from common import common_utils

LINEAR_HELP_STRING = 'python linear.py --train-filename=[train.csv] --predict-filename=[predict.csv]\n \
    <--test-filename=[test.csv]> <--enable-logging> <--enable-normal> <--model-dir=[/tmp/model_dir]> \n \
    <--load-model-dir=[/tmp/model_dir]> <--steps=[5000]> <--hidden-layers=[2]> <--dnn-units=[10]> \n \
    --output-dir=[/tmp/storage/]'

def input_fn(data_set, features, label):
    feature_cols = {k: tf.constant(data_set[k].values) for k in features}
    labels = tf.constant(data_set[label].values)
    return feature_cols, labels


def main(argv):

    enable_logging = False
    model_dir = None
    load_from_model_dir = None
    train_filename = None
    test_filename = None
    predict_filename = None
    disable_normalization = False

    for arg_item in argv:
        if (arg_item == common_utils.CMD_OPT_ENABLE_LOGGING_STR):
            enable_logging = True
        elif (arg_item.startswith(common_utils.CMD_OPT_MODEL_DIR_STR)):
            model_dir = arg_item.split(common_utils.DELIMITER_EQUAL)[1]
        elif (arg_item.startswith(common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR)):
            load_from_model_dir = arg_item.split(common_utils.DELIMITER_EQUAL)[1]
        elif (arg_item.startswith(common_utils.CMD_OPT_TRAIN_FILENAME)):
            train_filename = arg_item.split(common_utils.DELIMITER_EQUAL)[1]
        elif (arg_item.startswith(common_utils.CMD_OPT_TEST_FILENAME)):
            test_filename = arg_item.split(common_utils.DELIMITER_EQUAL)[1]
        elif (arg_item.startswith(common_utils.CMD_OPT_PREDICT_FILENAME)):
            predict_filename = arg_item.split(common_utils.DELIMITER_EQUAL)[1]
        elif (arg_item == common_utils.CMD_OPT_DISABLE_NORMALIZATION):
            disable_normalization = True
        elif (arg_item.startswith(common_utils.CMD_OPT_STEPS)):
            common_utils.TRAINING_STEPS = min(int(arg_item.split(common_utils.DELIMITER_EQUAL)[1]), common_utils.MAX_TRAINING_STEPS)
        elif (arg_item.startswith(common_utils.CMD_OPT_HIDDEN_LAYERS)):
            common_utils.HIDDEN_LAYERS = min(int(arg_item.split(common_utils.DELIMITER_EQUAL)[1]), common_utils.MAX_HIDDEN_LAYERS)
        elif (arg_item.startswith(common_utils.CMD_OPT_DNN_UNITS)):
            common_utils.DNN_UNITS = min(int(arg_item.split(common_utils.DELIMITER_EQUAL)[1]), common_utils.MAX_DNN_UNITS)
        elif (arg_item.startswith(common_utils.CMD_OPT_OUTPUT_DIR)):
            common_utils.OUTPUT_DIR = arg_item.split(common_utils.DELIMITER_EQUAL)[1]

    if common_utils.OUTPUT_DIR == None:
        common_utils.exit_with_error(LINEAR_HELP_STRING)
    if train_filename == None:
        if predict_filename == None:
            common_utils.exit_with_error(LINEAR_HELP_STRING)
        else:
            if load_from_model_dir == None:
                common_utils.exit_with_error(LINEAR_HELP_STRING)

    columns = None
    features = None
    label = None
    if train_filename != None:
        (columns, features, label, err, common_utils.DELIMITER_INPUT_TRAIN, common_utils.DELIMITER_INPUT_PREDICT) = \
            common_utils.define_column_feature_label(train_filename, predict_filename)
        if (err != None):
            common_utils.exit_with_error(err)

    if test_filename != None and common_utils.verify_column_by_filename(columns, test_filename) == False:
        common_utils.exit_with_error(common_utils.MESSAGE_ERROR_MISMATCHED_COLUMNS % (test_filename, train_filename),
                                     common_utils.MESSAGE_ERROR_MISMATCHED_COLUMNS_CODE)
    if enable_logging == True:
        tf.logging.set_verbosity(tf.logging.INFO)

    train_normalized_list = []

    temp_train_filename = train_filename
    temp_predict_filename = predict_filename
    temp_test_filename = test_filename

    # check if it's int and float only.  Otherwise, still need to disable normalization for now
    if (temp_train_filename != None):
        (valid, err, common_utils.DELIMITER_INPUT_TRAIN) = common_utils.verify_int_float_data_by_filename(temp_train_filename)
        if valid == False:
            disable_normalization = True

    if (temp_predict_filename != None):
        (valid, err, common_utils.DELIMITER_INPUT_PREDICT) = common_utils.verify_int_float_data_by_filename(temp_predict_filename)
        if valid == False:
            disable_normalization = True

    if (temp_test_filename != None):
        (valid, err, common_utils.DELIMITER_INPUT_TEST) = common_utils.verify_int_float_data_by_filename(temp_test_filename)
        if valid == False:
            disable_normalization = True

    if disable_normalization == False and train_filename != None:
        (temp_train_filename, train_normalized_list, err) = common_utils.normalize_inputfile(train_filename, common_utils.NORMALIZE_CONSTRAINT)
        if err != None:
            common_utils.exit_with_error(err)

        (temp_predict_filename, err) = common_utils.apply_normalize_inputfile(predict_filename, train_normalized_list)
        if err != None:
            common_utils.exit_with_error(err)

        (temp_test_filename, err) = common_utils.apply_normalize_inputfile(test_filename, train_normalized_list)
        if err != None:
            common_utils.exit_with_error(err)
    elif train_filename != None:
        pass

    # Load datasets
    if (temp_train_filename != None):
        training_set = pd.read_csv(temp_train_filename, skipinitialspace=True,
                                 skiprows=1, names=columns, delimiter=common_utils.DELIMITER_INPUT_TRAIN)

    if load_from_model_dir != None:
        #(columns, train_normalized_list, err) = common_utils.load_metadata_from_model(load_from_model_dir)
        (metadata_dict, err) = common_utils.load_metadata_from_model(load_from_model_dir)
        common_utils.TRAINING_STEPS = int(metadata_dict[common_utils.METADATA_TAG_STEPS][0])
        common_utils.HIDDEN_LAYERS = int(metadata_dict[common_utils.METADATA_TAG_HIDDEN_LAYERS][0])
        common_utils.DNN_UNITS = int(metadata_dict[common_utils.METADATA_TAG_DNN_UNITS][0])
        columns = metadata_dict[common_utils.METADATA_TAG_COLUMNS]
        if metadata_dict.has_key(common_utils.METADATA_TAG_NORMALIZATION_LIST):
            train_normalized_list = metadata_dict[common_utils.METADATA_TAG_NORMALIZATION_LIST]
        if err != None:
            common_utils.exit_with_error(err)
        label = columns[len(columns) - 1]
        (temp_predict_filename, err) = common_utils.apply_normalize_inputfile(temp_predict_filename, train_normalized_list)
        if err != None:
            common_utils.exit_with_error(err)

    prediction_set = None
    if (temp_predict_filename != None):
        if features == None:
            (_, features, _, err, _, common_utils.DELIMITER_INPUT_PREDICT) = \
                common_utils.define_column_feature_label(None, temp_predict_filename)
            if (err != None):
                common_utils.exit_with_error(err)

        prediction_set = pd.read_csv(temp_predict_filename, skipinitialspace=True,
                                   skiprows=1, names=columns, delimiter=common_utils.DELIMITER_INPUT_PREDICT)

    # Feature cols
    feature_cols = [tf.contrib.layers.real_valued_column(k)
                  for k in features]

    # Build hidden layers fully connected DNN with units.
    hidden_layers = []
    for layer_no in range(common_utils.HIDDEN_LAYERS):
      hidden_layers.append(common_utils.DNN_UNITS)

    if load_from_model_dir == None:
        try:
            if model_dir != None and common_utils.check_model_exists(model_dir) == True:
                common_utils.exit_with_error(common_utils.MESSAGE_ERROR_MODEL_ALREADY_EXISTS % model_dir)
            model_result_dir = common_utils.get_model_result_dirname(model_dir)
            regressor = tf.contrib.learn.DNNRegressor(
              feature_columns=feature_cols, hidden_units=hidden_layers, model_dir=model_result_dir)
        # Fit
            regressor.fit(input_fn=lambda: input_fn(training_set, features, label), steps=common_utils.TRAINING_STEPS)
        except tf.python.framework.errors.UnimplementedError as e:
            common_utils.exit_with_error(e.message)
        except:
            error = sys.exc_info()
            common_utils.exit_with_error(error[1].message)

        # if model_dir != None:
        #     # save columns and train_normalized_list
        #     (successful, err) = common_utils.create_meta(model_dir, columns, train_normalized_list)
        #     if err != None:
        #         common_utils.exit_with_error(err)
    else:
        final_load_from_model_dir = ''
        if common_utils.OUTPUT_DIR != None:
            final_load_from_model_dir = common_utils.OUTPUT_DIR
        final_load_from_model_dir += common_utils.BASE_MODEL_FOLDER + load_from_model_dir
        final_load_from_model_dir += common_utils.DELIMITER_DIRECTORY + common_utils.MODEL_RESULT_FOLDER
        regressor = tf.contrib.learn.DNNRegressor(model_dir=final_load_from_model_dir,
            feature_columns=feature_cols, hidden_units=hidden_layers)

    # Score accuracy
    loss_score = None
    if temp_test_filename != None:

        test_set = pd.read_csv(temp_test_filename, skipinitialspace=True,
                               skiprows=1, names=columns, delimiter=common_utils.DELIMITER_INPUT_TEST)

        ev = regressor.evaluate(input_fn=lambda: input_fn(test_set, features, label), steps=1)
        loss_score = ev[common_utils.LABEL_TEST_LOSS]
        print(common_utils.LABEL_TEST_LOSS + ':')
        if len(train_normalized_list) > 0:
            loss_score = loss_score * train_normalized_list[len(train_normalized_list) - 1]
        print("{0:f}".format(loss_score ))

    if load_from_model_dir == None and model_dir != None:
        # save columns and train_normalized_list
        # (successful, err) = common_utils.create_meta(model_dir, columns, train_normalized_list)
        metadata_dict = {}
        metadata_dict[common_utils.METADATA_TAG_COLUMNS] = common_utils.list_to_string(columns)
        metadata_dict[common_utils.METADATA_TAG_NORMALIZATION_LIST] = \
            common_utils.list_to_string(train_normalized_list)
        metadata_dict[common_utils.METADATA_TAG_STEPS] = str(common_utils.TRAINING_STEPS)
        metadata_dict[common_utils.METADATA_TAG_HIDDEN_LAYERS] = str(common_utils.HIDDEN_LAYERS)
        metadata_dict[common_utils.METADATA_TAG_DNN_UNITS] = str(common_utils.DNN_UNITS)
        if (loss_score != None):
            metadata_dict[common_utils.METADATA_TAG_DNN_REGRESSOR_LOSS] = str(loss_score)
        err = common_utils.create_meta(model_dir, metadata_dict)

        if err != None:
            common_utils.exit_with_error(err)

    # Print out predictions
    if (prediction_set is not None):
        y2 = regressor.predict(input_fn=lambda: input_fn(prediction_set, features, label))
        y = list(itertools.islice(y2, len(prediction_set)))

        # save to file and output
        with open(common_utils.OUTPUT_DIR + common_utils.OUTPUT_FILENAME, 'w') as output_file:

            #output_file.write(common_utils.LABEL_PREDICTION+':\n')
            print(common_utils.LABEL_PREDICTION+':'+str(label))
            for item in y:
                if len(train_normalized_list) > 0:
                    output_file.write(str(item * train_normalized_list[len(train_normalized_list) - 1]))
                    print(item * train_normalized_list[len(train_normalized_list) - 1])

                else:
                    output_file.write(str(item))
                    print(item)
                output_file.write('\n')

if __name__ == "__main__":
    tf.app.run()
    sys.exit(0)
