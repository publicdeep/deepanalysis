# -*- coding: utf-8 -*-
#  Copyright 2016 All Rights Reserved.
#
"""Common Utils"""

import os
import subprocess
import sys
import numpy as np
import csv
import random
import string
import re

# from dict to json: http://stackoverflow.com/questions/8230315/python-sets-are-not-json-serializable
from json import dumps, loads, JSONEncoder, JSONDecoder
import pickle

class PythonObjectEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (list, dict, str, unicode, int, float, bool, type(None))):
            return JSONEncoder.default(self, obj)
        return {'_python_object': pickle.dumps(obj)}

def as_python_object(dct):
    if '_python_object' in dct:
        return pickle.loads(str(dct['_python_object']))
    return dct

DELIMITER_COMMA = ','
DELIMITER_TAB = '\t'
DELIMITER_EQUAL = '='
DELIMITER_DIRECTORY = '/'

DELIMITER_INPUT_TRAIN=None
DELIMITER_INPUT_TEST=None
DELIMITER_INPUT_PREDICT=None

TRAINING_STEPS=1000
DNN_UNITS = 10
HIDDEN_LAYERS = 2
TRAINING_MODE_WIDE='wide'
TRAINING_MODE_DEEP='deep'

MAX_TRAINING_STEPS=50000
MAX_DNN_UNITS = 100
MAX_HIDDEN_LAYERS = 5

# these command line options must be identical with the same section in deep_backend/common/constants.py
CMD_OPT_ENABLE_LOGGING_STR = '--enable-logging'
CMD_OPT_DISABLE_NORMALIZATION = '--disable-normal'
CMD_OPT_MODEL_DIR_STR = '--model_dir'
CMD_OPT_LOAD_FROM_MODEL_DIR_STR = '--load-model-dir'
CMD_OPT_TRAIN_FILENAME='--train-filename'
CMD_OPT_TEST_FILENAME='--test-filename'
CMD_OPT_PREDICT_FILENAME='--predict-filename'
CMD_OPT_STEPS='--steps'
CMD_OPT_HIDDEN_LAYERS='--hidden-layers'
CMD_OPT_DNN_UNITS='--dnn-units'
CMD_OPT_OUTPUT_DIR='--output-dir'
CMD_OPT_IMGNET_IMAGE_FILE='--image_file'

LABEL_TEST_LOSS = 'loss' # This cannot be changed, as it's the official output from DNNRegressor
LABEL_TEST_ACCURACY = 'accuracy'
LABEL_PREDICTION = 'prediction'
LABEL_CLASSIFICATION = 'classification'

MESSAGE_ERROR_MISMATCHED_COLUMNS_CODE = 2
OUTPUT_FILENAME = 'output.csv'
BASE_MODEL_FOLDER = 'model/'

TAG_IMAGENET_SCORE='score'

MODEL_METADATA_FOLDER = 'meta/'
MODEL_METADATA_FILENAME = 'meta.txt'

METADATA_TAG_COLUMNS = 'COLUMNS'
METADATA_TAG_DNN_REGRESSOR_LOSS='dnn_regressor_loss'
METADATA_TAG_DNN_CLASSIFICATION_ACCURACY='dnn_classification_accuracy'
METADATA_TAG_SPARSE_COLUMNS = 'SPARSE_COLUMNS'
METADATA_TAG_CONTINUOUS_COLUMNS = 'CONTINUOUS_COLUMNS'
METADATA_TAG_SPARSE_VALUES_DICT = 'SPARSE_VALUES_DICT'

# end of declarations that needs to go to deep_backend/common/constants.py

MESSAGE_ERROR_MISMATCHED_COLUMNS_IN_SAME_FILE = 'Following files do not have identical columns: "%s"'

MESSAGE_ERROR_MISMATCHED_COLUMNS = 'Following files do not have identical columns: "%s" and "%s"'

MESSAGE_ERROR_TOO_FEW_COLUMNS = 'Training file "%s" needs at least 2 columns.'
MESSAGE_ERROR_INVALID_DATA_TYPE = 'Error: invalid data in "%s".  Only support int or float, include negative.'

MESSAGE_ERROR_MISMATCHED_PREDICT_COLUMNS = 'Predict file "%s" should have one less column than training file "%s"'
MESSAGE_ERROR_MISMATCHED_VALUE_IN_LABEL_COLUMN_WITH_TRAINING_FILE = \
    'Input file "%s" has a mismatched label value "%s" with training file'
MESSAGE_ERROR_MODEL_ALREADY_EXISTS = 'Model "%s" already exists.'
MESSAGE_ERROR_MODEL_NOT_EXISTS = 'Model "%s" does not exist.'
MESSAGE_ERROR_OUTPUT_DIR_NOT_EXISTS = 'Must sepcify "--output-dir" in command line'
MESSAGE_ERROR_CONTAINS_MIXED_DELIMITER = 'Input: "%s" contains mixed delimiters'
MESSAGE_ERROR_CONTAINS_INVALID_DELIMITER = 'Input: "%s" contains invalid delimiters'
MESSAGE_ERROR_FILE_INVALID_INPUT = 'Inputfile: "%s" contains invalid input'

NORMALIZATION_PREFIX = 'dnormalized_'
NORMALIZE_CONSTRAINT = 10
MODEL_RESULT_FOLDER = 'result'
OUTPUT_DIR = None

METADATA_TAG_NORMALIZATION_LIST = 'NORMALIZATION_LIST'
METADATA_TAG_STEPS='STEPS'
METADATA_TAG_DNN_UNITS='DNN_UNITS'
METADATA_TAG_HIDDEN_LAYERS='HIDDEN_LAYERS'
METADATA_TAG_LABEL_LIST='LABEL_LIST'

POSTFIX_RELABEL = '_relabel' # used for relabel DNN classifier input file

def define_column_feature_label(training_filename, predict_filename):
    column_list = []
    feature_list = []
    label = None
    delimiter_train = None
    delimiter_predict = None
    if (training_filename != None):
        with open(training_filename, "r") as train_file:
            for line in train_file:
                line = line.strip()
                if DELIMITER_COMMA in line:
                    column_list = line.split(DELIMITER_COMMA)
                    if delimiter_train != None and delimiter_train != DELIMITER_COMMA:
                        return None, None, None, MESSAGE_ERROR_CONTAINS_MIXED_DELIMITER % \
                               extract_from_last_occurence_of_string(training_filename, '/'), None, None
                    delimiter_train = DELIMITER_COMMA
                elif DELIMITER_TAB in line:
                    column_list = line.split(DELIMITER_TAB)
                    if delimiter_train != None and delimiter_train != DELIMITER_TAB:
                        return None, None, None, MESSAGE_ERROR_CONTAINS_MIXED_DELIMITER % \
                               extract_from_last_occurence_of_string(training_filename, '/'), None, None
                    delimiter_train = DELIMITER_TAB
                else:
                    # the train file must has 2 columns, 1 for feature, 1 for label
                    return None, None, None, MESSAGE_ERROR_TOO_FEW_COLUMNS % \
                           extract_from_last_occurence_of_string(training_filename, '/'), None, None
                break

        feature_list = column_list[:len(column_list)-1]
        label = column_list[len(column_list)-1]

        if predict_filename != None and verify_column_by_filename(feature_list, predict_filename) == False:
            return None, None, None, MESSAGE_ERROR_MISMATCHED_PREDICT_COLUMNS % (predict_filename, training_filename), None, None

    elif predict_filename != None:
        with open(predict_filename, "r") as predict_file:
            for line in predict_file:
                line = line.strip()
                if DELIMITER_COMMA in line:
                    column_list = line.split(DELIMITER_COMMA)
                    if delimiter_predict != None and delimiter_predict != DELIMITER_COMMA:
                        return None, None, None, MESSAGE_ERROR_CONTAINS_MIXED_DELIMITER % predict_filename, None, None
                    delimiter_predict = DELIMITER_COMMA

                elif DELIMITER_TAB in line:
                    column_list = line.split(DELIMITER_TAB)
                    if delimiter_predict != None and delimiter_predict != DELIMITER_TAB:
                        return None, None, None, MESSAGE_ERROR_CONTAINS_MIXED_DELIMITER % predict_filename, None, None
                    delimiter_predict = DELIMITER_TAB
                else:
                    # the train file must has 2 columns, 1 for feature, 1 for label
                    return None, None, None, MESSAGE_ERROR_TOO_FEW_COLUMNS % predict_filename, None, None
                break
        feature_list = column_list[:]
        label = None

    return column_list, feature_list, label, None, delimiter_train, delimiter_predict


def verify_column_by_filename(in_columns_list, filename):
    column_list = []
    with open(filename, "r") as input_file:
        for line in input_file:
            line = line.strip()
            if DELIMITER_COMMA in line:
                column_list = line.split(DELIMITER_COMMA)
            elif DELIMITER_TAB in line:
                column_list = line.split(DELIMITER_TAB)
            else:
                column_list.append(line)
            break

    if (in_columns_list != column_list):
        return False

    return True

def verify_int_float_data_by_filename(filename, skip_label_field=False):
    first_line = True
    column_list = []
    delimiter = None
    with open(filename, "r") as input_file:
        for line in input_file:
            if first_line == True:
                # skip first line, which are column names
                first_line = False
                continue
            line = line.strip()
            if DELIMITER_COMMA in line:
                column_list = line.split(DELIMITER_COMMA)
                if delimiter != None and delimiter != DELIMITER_COMMA:
                    return False, MESSAGE_ERROR_CONTAINS_MIXED_DELIMITER % filename, None
                delimiter = DELIMITER_COMMA

            elif DELIMITER_TAB in line:
                column_list = line.split(DELIMITER_TAB)
                if delimiter != None and delimiter != DELIMITER_TAB:
                    return False, MESSAGE_ERROR_CONTAINS_MIXED_DELIMITER % filename, None
                delimiter = DELIMITER_TAB

            else:
                column_list.append(line)

            #for item in column_list:
            column_length = len(column_list)
            for item_index in range(column_length):
                if skip_label_field == True and item_index == column_length - 1:
                    break
                item = (column_list[item_index]).strip()
                # http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
                if item.lstrip('-').replace('.','',1).isdigit() == False:
                    return False, MESSAGE_ERROR_INVALID_DATA_TYPE % filename, delimiter

    return True, None, delimiter

def is_int_or_float_or_scientific(item):
    if item.lstrip('-').replace('.','',1).isdigit() == True:
        return True

    re_match = re.compile(r'^([0-9]+[.]?[0-9]*|[0-9]*[.][0-9]+)[eE][+-]?[0-9]+$').match
    if bool(re_match(item)) == True:
        return True

    return False

def normalize_inputfile(filename, normalize_contraint, skip_label_field=False):
# this function should NOT verify validity of input file, as it should be verified before
    first_line = True
    column_average_list = []
    column_normalize_index_list = []
    has_negative = False
    actual_delimiter = None
    tempfile_headerline = None

    cached_input_list = [] # cache it, or read text file one more time? Choose performance for now

    with open(filename, "r") as input_file:
        column_str_list = []
        for line in input_file:
            if first_line == True:
                # skip first line, which are column names
                first_line = False
                tempfile_headerline = line
                continue
            line = line.strip()
            if DELIMITER_COMMA in line:
                column_str_list = line.split(DELIMITER_COMMA)
                actual_delimiter = DELIMITER_COMMA
            elif DELIMITER_TAB in line:
                column_str_list = line.split(DELIMITER_TAB)
                actual_delimiter = DELIMITER_TAB
            else:
                column_str_list.append(line)

            column_index = 0
            cached_line_list = []
            column_length = len(column_str_list)
            for column_index in range(column_length):
                item=(column_str_list[column_index]).strip()
                if (skip_label_field==True and column_index == column_length-1):
                    cached_line_list.append(item)
                    break
                # http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
                if item.lstrip('-').replace('.', '', 1).isdigit() == False:
                    return None, None, MESSAGE_ERROR_INVALID_DATA_TYPE % filename

                converted_value = float(item)
                cached_line_list.append(converted_value)

                if (converted_value < 0.0):
                    has_negative = True

                if ((len(column_str_list) - len(column_average_list)) > 1):
                        column_average_list.append(converted_value)
                elif (len(column_str_list) - len(column_average_list)) == 1 and skip_label_field == False:
                    column_average_list.append(converted_value)
                else:
                    column_average_list[column_index] += converted_value
                    column_index += 1

            cached_input_list.append(cached_line_list)

    if len(column_average_list) == 0:
        return None, None, MESSAGE_ERROR_FILE_INVALID_INPUT % filename

    normalize_list = []
    if has_negative == False:
        # the problem with mixing negative is: there is no way to normalize linearly with same magtitude
        # use first column as reference point.
        reference_average = column_average_list[0]
        for item in column_average_list:
            normalize_list.append(get_normalize_rate(reference_average, item, normalize_contraint))

    temp_filename = None
    if len(normalize_list) > 0:
        temp_filename = create_temp_filename(NORMALIZATION_PREFIX, filename)
        create_normalized_file(temp_filename, tempfile_headerline, cached_input_list, actual_delimiter, normalize_list, skip_label_field)

        return temp_filename, normalize_list, None

    return filename, normalize_list, None


def get_normalize_rate(reference, item, normalize_contraint):
    if (reference <= 0.0 or item <= 0.0):
        # do not normalize anything that has negative number
        return 1.0

    magtitude = 10
    temp_item = item
    normalize_rate = 1.0
    if (reference < temp_item):
        while (reference < temp_item):
            # need to increase item
            if ((temp_item / normalize_contraint) <= reference):
                break
            normalize_rate = normalize_rate * magtitude
            temp_item = temp_item / magtitude
    else:
        while (reference > temp_item):
            # need to increase item
            if ((temp_item * normalize_contraint) >= reference):
                break
            normalize_rate = normalize_rate / magtitude
            temp_item = temp_item * magtitude

    return normalize_rate

def apply_normalize_inputfile(filename, normalize_list, skip_label_field=False):
# this function should NOT verify validity of input file, as it should be verified before
    if filename == None or len(normalize_list) == 0:
        return filename, None

    first_line = True
    #column_average_list = []
    has_negative = False
    actual_delimiter = None
    tempfile_headerline = None

    cached_input_list = [] # cache it, or read text file one more time? Choose performance for now
    column_length = 0

    with open(filename, "r") as input_file:
        column_str_list = []
        for line in input_file:
            if first_line == True:
                # skip first line, which are column names
                first_line = False
                tempfile_headerline = line
                continue
            line = line.strip()
            if DELIMITER_COMMA in line:
                column_str_list = line.split(DELIMITER_COMMA)
                actual_delimiter = DELIMITER_COMMA
            elif DELIMITER_TAB in line:
                column_str_list = line.split(DELIMITER_TAB)
                actual_delimiter = DELIMITER_TAB
            else:
                column_str_list.append(line)

            cached_line_list = []
            column_length = len(column_str_list)
            #for item in column_str_list:
            for column_index in range (column_length):
                item = (column_str_list[column_index]).strip()
                if skip_label_field == True and column_index == column_length-1:
                    cached_line_list.append(item)
                    break
                # http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
                if item.lstrip('-').replace('.', '', 1).isdigit() == False:
                    return None, MESSAGE_ERROR_INVALID_DATA_TYPE % filename

                converted_value = float(item)
                cached_line_list.append(converted_value)

                if (converted_value < 0.0):
                    has_negative = True

            cached_input_list.append(cached_line_list)

    if has_negative == True:
        return filename, None

    temp_filename = None
    if len(normalize_list) > 0 and column_length > 0:
        temp_filename = create_temp_filename(NORMALIZATION_PREFIX, filename)
        create_normalized_file(temp_filename, tempfile_headerline, cached_input_list, actual_delimiter,
                               normalize_list, skip_label_field)

        return temp_filename, None

    return filename, None

def create_normalized_file(filename, tempfile_headerline, cached_input_list, actual_delimiter, normalize_list, skip_label_field=False):

    with open(filename, "w") as normalized_file:

        # create temporary normalized file
        normalized_file.write(tempfile_headerline.rstrip()+'\n')
        for line in cached_input_list:
            item_index = 0
            for item_index in range(len(line)):
                item = line[item_index]
                if skip_label_field == True and item_index == len(line)-1:
                    new_item = item
                else:
                    new_item = item * (1.0 / normalize_list[item_index])
                if (item_index == 0):
                    normalized_file.write(str(new_item))
                else:
                    normalized_file.write(actual_delimiter + str(new_item))
                item_index += 1
            normalized_file.write('\n')
        normalized_file.close()

def create_temp_filename(prefix, filename):
    last_dir_delimiter_index = filename.rfind(DELIMITER_DIRECTORY)
    if (last_dir_delimiter_index < 0):
        return prefix + filename

    return filename[:last_dir_delimiter_index] + DELIMITER_DIRECTORY + prefix + filename[last_dir_delimiter_index+1:]

def exit_with_error(err, err_code=1):
    print (err)
    sys.exit(err_code)

def check_model_exists(model_name):
    model_dir = ''
    if OUTPUT_DIR != None:
        model_dir = OUTPUT_DIR

    model_dir += BASE_MODEL_FOLDER + model_name

    if os.path.exists(model_dir):
        return True
    return False

def create_meta(model_dir, metadata_dict):
    if (model_dir == None or metadata_dict == None):
        return None
    # 1) create common_utils.BASE_MODEL_FOLDER + model_dir + "meta"
    model_meta_dir = ''
    if OUTPUT_DIR != None:
        model_meta_dir = OUTPUT_DIR

    model_meta_dir += BASE_MODEL_FOLDER + model_dir + '/' + MODEL_METADATA_FOLDER

    if os.path.exists(model_meta_dir):
        return MESSAGE_ERROR_MODEL_ALREADY_EXISTS % model_dir

    os.makedirs(model_meta_dir)

    meta_filename = model_meta_dir + MODEL_METADATA_FILENAME
    with open(meta_filename, 'w') as temp_file:
        for key in metadata_dict.keys():
            if len(metadata_dict[key]) > 0:
                temp_file.write(key + ':' + metadata_dict[key] + '\n')

    return None

def get_model_result_dirname(model_dir):
    if (model_dir == None):
        return None

    model_result_dir = ''
    if OUTPUT_DIR != None:
        model_result_dir = OUTPUT_DIR

    model_result_dir += BASE_MODEL_FOLDER + model_dir + '/' + MODEL_RESULT_FOLDER
    return model_result_dir

def list_to_string(in_list, delimiter=DELIMITER_COMMA):
    return_string = ''
    is_first_line = True
    for item in in_list:
        tmp_string = ''
        if type(item) is list:
            return None
            #tmp_string = list_to_string(item, delimiter)
        elif type(item) is dict:
            return None
            #tmp_string = dict_to_string(item, delimiter)
        elif type(item) is set:
            return None
            #tmp_string = list_to_string(item, delimiter)
        else:
            tmp_string = str(item)

        if is_first_line == True:
            return_string += tmp_string
            is_first_line = False
        else:
            return_string += delimiter + tmp_string
    return return_string

# def dict_to_string(in_dict, delimiter=DELIMITER_COMMA):
#     return_string = ''
#     is_first_line = True
#     for key in in_dict.keys():
#         return_string += key + ':'
#         if type(in_dict[key]) is list:
#             return None
#             #return_string += list_to_string(in_dict[key], delimiter)
#         elif type(in_dict[key]) is dict:
#             return None
#             #return_string += dict_to_string(in_dict[key], delimiter)
#         elif type(in_dict[key]) is set:
#             return None
#             #return_string += list_to_string(in_dict[key], delimiter)
#         else:
#             return_string += str(in_dict[key])
#         return_string += ';'
#
#     return return_string

def load_metadata_from_model(model_dir):
    model_meta_dir = ''
    if OUTPUT_DIR != None:
        model_meta_dir = OUTPUT_DIR

    model_meta_dir += BASE_MODEL_FOLDER + model_dir + '/' + MODEL_METADATA_FOLDER
    meta_filename = model_meta_dir + MODEL_METADATA_FILENAME

    if os.path.exists(meta_filename) == False:
        return None, MESSAGE_ERROR_MODEL_NOT_EXISTS % model_dir

    metadata_dict = {}
    with open(meta_filename, 'r') as meta_file:
        line_index = 0
        for line in meta_file:
            line = line.strip()
            tag_content_list = line.split(':', 1)
            if tag_content_list[0] != METADATA_TAG_NORMALIZATION_LIST:
                metadata_dict[tag_content_list[0]] = tag_content_list[1].split(DELIMITER_COMMA)
            else:
                normalize_list = []
                temp_list = tag_content_list[1].split(DELIMITER_COMMA)
                for item in temp_list:
                    normalize_list.append(float(item))
                metadata_dict[tag_content_list[0]] = normalize_list

    return metadata_dict, None

def execute_system_call(command_list, standard_input = None):
    try:
        output = subprocess.check_output(command_list, stdin=standard_input, shell=False)
    except subprocess.CalledProcessError as e:
        return (None, e.returncode, e.output)
    except:
        error = sys.exc_info()
        return (None, error[1].args[0], error[1].args[1])


    return (output, 0, None)

def get_dataset(filename,
                target_dtype,
                features_dtype,
                n_samples,
                n_features,
                target_column=-1):

    data = None
    target = None

    delimiter = DELIMITER_COMMA
    with open(filename, 'r') as data_set:
        delimiter=DELIMITER_COMMA
        for line in data_set:
            line = line.strip()
            if DELIMITER_COMMA in line:
                break
            elif DELIMITER_TAB in line:
                delimiter = DELIMITER_TAB
                break
            else:
                return None, None

    with open(filename, 'r') as data_set:
        data_file = csv.reader(data_set, delimiter=delimiter)
        next(data_file)
        data = np.zeros((n_samples, n_features), dtype=features_dtype)
        target = np.zeros((n_samples,), dtype=target_dtype)
        for i, row in enumerate(data_file):
            target[i] = np.asarray(row.pop(target_column), dtype=target_dtype)
            data[i] = np.asarray(row, dtype=features_dtype)

    return (data, target)

def get_samples_features_count(filename, skip_firstline=True):
    samples_count = None
    feature_count = None
    delimiter = DELIMITER_COMMA
    label_sets = set()
    with open(filename, 'r') as data_set:
        for line in data_set:
            if samples_count == None:
                if DELIMITER_COMMA in line:
                    pass
                elif DELIMITER_TAB in line:
                    delimiter = DELIMITER_TAB
                else:
                    return None, None, None, None
                feature_count = len(line.split(delimiter)) - 1
                samples_count = 0
            column_list = line.split(delimiter)
            if samples_count == 0 and skip_firstline == False:
                label_sets.add((column_list[len(column_list) - 1]).strip())
            elif samples_count > 0:
                label_sets.add((column_list[len(column_list) - 1]).strip())
            samples_count += 1


    if samples_count > 0 and skip_firstline:
        samples_count -= 1

    return samples_count, feature_count, delimiter, label_sets

def load_data_to_list_of_list(filename, skip_firstline=True):
    line_count = 0
    delimiter = DELIMITER_COMMA
    return_list = []
    line_list = None
    with open(filename, 'r') as input_file:
        for line in input_file:
            line = line.strip()
            if line_count == 0:
                if DELIMITER_COMMA in line:
                    pass
                elif DELIMITER_TAB in line:
                    delimiter = DELIMITER_TAB
                else:
                    return None, None
                if skip_firstline == False:
                    line_list = map(float, line.split(delimiter)) # split to float
            else:
                line_list = map(float, line.split(delimiter))  # split to float
            if line_list != None:
                return_list.append(line_list)

            line_count += 1

    return return_list, delimiter

# def random_str(length=20):
#     return_str = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(length))
#     return return_str

def add_postfix_in_last_occurence_of_string(in_str, postfix_str, occurence_str):
    k = in_str.rfind(occurence_str)
    new_string = in_str[:k] + postfix_str + in_str[k:]
    return new_string

def remove_last_occurence_of_string(in_str, occurence_str):
    k = in_str.rfind(occurence_str)
    new_string = in_str[:k] + in_str[k + len(occurence_str):]
    return new_string

def extract_from_last_occurence_of_string(in_str, occurence_str):
    k = in_str.rfind(occurence_str)
    new_string = in_str[k + len(occurence_str):]
    return new_string

# def get_label_list(filename, skip_firstline=True):
#     label_set = set()
#     with open(filename, 'r') as in_file:
#         line_count = 0
#         delimiter = DELIMITER_COMMA
#         for line in in_file:
#             line = line.strip()
#             line_list = []
#             if line_count == 0:
#                 if DELIMITER_COMMA in line:
#                     pass
#                 elif DELIMITER_TAB in line:
#                     delimiter = DELIMITER_TAB
#                 else:
#                     return None, None
#                 if skip_firstline == False:
#                     line_list = line.split(delimiter)
#                     last_label = line_list[len(line_list) - 1].strip()
#                     label_set.add(last_label)
#
#             else:
#                 line_list = line.split(delimiter)
#                 last_label = line_list[len(line_list) - 1].strip()
#                 label_set.add(last_label)
#             line_count += 1
#
#     # since set doesn't support index, convert label_set to list, in order to get mapped index
#     return list(label_set)

def map_label_field_to_int(filename, skip_firstline=True):
    out_filename = add_postfix_in_last_occurence_of_string(filename, POSTFIX_RELABEL, '.')
    label_list = []
    with open(filename, 'r') as in_file, \
        open(out_filename, 'w') as out_file:
        line_count = 0
        delimiter = DELIMITER_COMMA
        last_label=None
        for line in in_file:
            line = line.strip()
            line_list = []
            if line_count == 0:
                if DELIMITER_COMMA in line:
                    pass
                elif DELIMITER_TAB in line:
                    delimiter = DELIMITER_TAB
                else:
                    return None, None
                if skip_firstline == False:
                    line_list = line.split(delimiter)
                    last_label = line_list[len(line_list) - 1].strip()
                    label_list.append(last_label)
                else:
                    out_file.write(line+'\n')
                    line_count += 1
                    continue

            else:
                line_list = line.split(delimiter)
                last_label = line_list[len(line_list) - 1].strip()
                if (last_label not in label_list):
                    label_list.append(last_label)
            # replace last item in line_list by index of label in label_set
            line_list[len(line_list) - 1] = label_list.index(last_label)
            out_file.write(list_to_string(line_list, delimiter) + '\n')
            line_count += 1

    return out_filename, label_list

def apply_map_label_field_to_int(filename, label_list, disable_normalization, skip_firstline=True):
    out_filename = add_postfix_in_last_occurence_of_string(filename, POSTFIX_RELABEL, '.')
    with open(filename, 'r') as in_file, \
        open(out_filename, 'w') as out_file:
        line_count = 0
        delimiter = DELIMITER_COMMA
        last_label=None
        for line in in_file:
            line = line.strip()
            line_list = []
            if line_count == 0:
                if DELIMITER_COMMA in line:
                    pass
                elif DELIMITER_TAB in line:
                    delimiter = DELIMITER_TAB
                else:
                    return None
                if skip_firstline == True:
                    out_file.write(line+'\n')
                    line_count += 1
                    continue
            else:
                line_list = line.split(delimiter)
                last_label = line_list[len(line_list) - 1].strip()
                if (last_label not in label_list):
                    actual_filename = filename
                    if disable_normalization == False:
                        actual_filename = remove_last_occurence_of_string(filename, NORMALIZATION_PREFIX)
                    actual_filename = extract_from_last_occurence_of_string(actual_filename, '/')
                    return None, MESSAGE_ERROR_MISMATCHED_VALUE_IN_LABEL_COLUMN_WITH_TRAINING_FILE % \
                           (actual_filename, last_label)

            # replace last item in line_list by index of label in label_set
            line_list[len(line_list) - 1] = label_list.index(last_label)
            out_file.write(list_to_string(line_list, delimiter) + '\n')
            line_count += 1

    return out_filename, None

# because first line is the column name
def get_sparse_columns_skip_firstline(filename):
    if filename==None:
        return None, None, None, None

    sparse_column_set = set()
    sparse_column_value_map = {}
    column_list = []
    is_firstline = True
    delimiter = DELIMITER_COMMA
    with open(filename, 'r') as input_file:
        for line in input_file:
            line = line.strip()

            if is_firstline:
                if DELIMITER_COMMA in line:
                    column_list = line.split(DELIMITER_COMMA)
                    delimiter = DELIMITER_COMMA
                elif DELIMITER_TAB in line:
                    column_list = line.split(DELIMITER_TAB)
                    delimiter = DELIMITER_TAB
                else:
                    return None, None, None, MESSAGE_ERROR_CONTAINS_INVALID_DELIMITER % \
                               extract_from_last_occurence_of_string(filename, '/')
                is_firstline = False
                continue
            line_column_list = line.split(delimiter)
            if len(line_column_list) != len(column_list):
                return None, None, None, MESSAGE_ERROR_MISMATCHED_COLUMNS_IN_SAME_FILE % \
                               extract_from_last_occurence_of_string(filename, '/')

            column_index = 0
            for item in line_column_list:
                item = item.strip()
                if is_int_or_float_or_scientific(item) == False:
                    sparse_column_set.add(column_list[column_index])
                    if sparse_column_value_map.has_key(column_list[column_index]) == False:
                        sparse_column_value_map[column_list[column_index]] = set()
                    sparse_column_value_map[column_list[column_index]].add(item)
                column_index += 1

        continuous_column_set = set()
        for item in column_list:
            if item not in sparse_column_set:
                continuous_column_set.add(item)

        return sparse_column_set, sparse_column_value_map, continuous_column_set, None



