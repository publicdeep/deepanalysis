# -*- coding: utf-8 -*-
#import mock
import unittest
import os
import numpy as np

import sys
sys.path.append('../../')
from common import common_utils
import shutil
# from dict to json: http://stackoverflow.com/questions/8230315/python-sets-are-not-json-serializable
from json import dumps, loads, JSONEncoder, JSONDecoder
import pickle

class TestClass1(unittest.TestCase):
    def setUp(self):
        return

    def tearDown(self):
        return

    def test_correct_columns(self):
        training_filename = 'data/train.csv'
        predict_filename = 'data/predict.csv'
        (columns, features, label, err, delimiter_train, delimiter_predict) = \
            common_utils.define_column_feature_label(training_filename, predict_filename)
        self.assertEqual(len(columns), 4)
        self.assertEqual(len(features), 3)
        self.assertEqual(err, None)
        self.assertEqual(label, 'SALES')
        self.assertEqual(delimiter_train, ',')
        self.assertEqual(delimiter_predict, None)

    def test_only_one_column(self):
        training_filename = 'data/train_error_1.csv'
        (columns, features, label, err, delimiter_train, delimiter_predict) = \
            common_utils.define_column_feature_label(training_filename, None)
        self.assertEqual(columns, None)
        self.assertEqual(features, None)
        self.assertEqual(label, None)
        self.assertEqual(err, common_utils.MESSAGE_ERROR_TOO_FEW_COLUMNS % 'train_error_1.csv')
        self.assertEqual(delimiter_train, None)
        self.assertEqual(delimiter_predict, None)

    def test_mismatched_column_names(self):
        training_filename = 'data/train_error_2.csv'
        predict_filename = 'data/predict.csv'
        (columns, features, label, err, delimiter_train, delimiter_predict) = \
            common_utils.define_column_feature_label(training_filename, predict_filename)
        self.assertEqual(columns, None)
        self.assertEqual(features, None)
        self.assertEqual(label, None)
        self.assertFalse(err == None)
        self.assertEqual(delimiter_train, None)
        self.assertEqual(delimiter_predict, None)

    def test_verify_column_by_filename(self):
        training_filename = 'data/train.csv'
        predict_filename = 'data/predict.csv'
        test_filename = 'data/test.csv'
        (columns, features, label, err, delimiter_train, delimiter_predict) = \
            common_utils.define_column_feature_label(training_filename, predict_filename)
        self.assertEqual(delimiter_train, ',')
        self.assertEqual(delimiter_predict, None)
        is_equal = common_utils.verify_column_by_filename(columns, test_filename)
        self.assertEqual(is_equal, True)

        is_equal = common_utils.verify_column_by_filename(features, test_filename)
        self.assertEqual(is_equal, False)
        is_equal = common_utils.verify_column_by_filename(None, test_filename)
        self.assertEqual(is_equal, False)


    def test_verify_int_float_data_by_filename(self):
        training_filename = 'data/train.csv' # combination of increase and decrease
        (valid, err, common_utils.DELIMITER_INPUT_TRAIN) = common_utils.verify_int_float_data_by_filename(
            training_filename)
        self.assertEqual(err, None)
        self.assertEqual(valid, True)
        self.assertEqual(common_utils.DELIMITER_INPUT_TRAIN, ',')

        training_filename = 'data/train_invalid_data.csv' # combination of increase and decrease
        (valid, err, common_utils.DELIMITER_INPUT_TRAIN) = common_utils.verify_int_float_data_by_filename(
            training_filename)
        self.assertEqual(err, common_utils.MESSAGE_ERROR_INVALID_DATA_TYPE % training_filename)
        self.assertEqual(valid, False)
        self.assertEqual(common_utils.DELIMITER_INPUT_TRAIN, common_utils.DELIMITER_COMMA)

        training_filename = 'data/train_normal_classify_label.csv' # combination of increase and decrease
        (valid, err, common_utils.DELIMITER_INPUT_TRAIN) = common_utils.verify_int_float_data_by_filename(
            training_filename, skip_label_field=True)
        self.assertEqual(err, None)
        self.assertEqual(valid, True)
        self.assertEqual(common_utils.DELIMITER_INPUT_TRAIN, ',')

        return

    def test_normalize_inputfile(self):
        training_filename = 'data/train_normal_1.csv' # combination of increase and decrease
        (temp_filename, normalized_list, err) = common_utils.normalize_inputfile(training_filename, common_utils.NORMALIZE_CONSTRAINT)
        file_content = ''
        with open(temp_filename, "r") as input_file:
            for line in input_file:
                file_content += line

        expected_content = 'DAY,FEMALE,MALE,SALES\n'
        expected_content += '3.0,0.62428,3.7572,20.49535\n'
        expected_content += '4.0,0.6443,3.557,35.38812\n'
        self.assertEqual(file_content, expected_content)
        os.remove(temp_filename)

        training_filename = 'data/train.csv' # no change, only from int to float
        (temp_filename, normalized_list, err) = common_utils.normalize_inputfile(training_filename,
                                                                                 common_utils.NORMALIZE_CONSTRAINT)
        file_content = ''
        with open(temp_filename, "r") as input_file:
            for line in input_file:
                file_content += line

        expected_content = 'DAY,FEMALE,MALE,SALES\n'
        expected_content += '3.0,0.62428,0.37572,20.49535\n'
        expected_content += '4.0,0.6443,0.3557,35.38812\n'
        self.assertEqual(file_content, expected_content)
        os.remove(temp_filename)

        training_filename = 'data/train_normal_2.csv' # negative, no change
        (temp_filename, normalized_list, err) = common_utils.normalize_inputfile(training_filename,
                                                                                 common_utils.NORMALIZE_CONSTRAINT)
        file_content = ''
        with open(temp_filename, "r") as input_file:
            for line in input_file:
                file_content += line

        expected_content = 'DAY,FEMALE,MALE,SALES\n'
        expected_content += '3,0.62428,3.7572,20.49535\n'
        expected_content += '4,0.6443,-3.557,35.38812\n'
        self.assertEqual(file_content, expected_content)

        training_filename = 'data/train_normal_3.csv'  # total is 0 for 2nd column, expect no normalization
        (temp_filename, normalized_list, err) = common_utils.normalize_inputfile(training_filename,
                                                                                 common_utils.NORMALIZE_CONSTRAINT)
        file_content = ''
        with open(temp_filename, "r") as input_file:
            for line in input_file:
                file_content += line

        expected_content = 'DAY,FEMALE,MALE,SALES\n'
        expected_content += '300.0,0.0,37.572,204.9535\n'
        expected_content += '400.0,0.0,35.57,353.8812\n'
        self.assertEqual(file_content, expected_content)
        os.remove(temp_filename)

        training_filename = 'data/train_normal_4.csv'  # 1 field has value of 0
        (temp_filename, normalized_list, err) = common_utils.normalize_inputfile(training_filename,
                                                                                 common_utils.NORMALIZE_CONSTRAINT)
        file_content = ''
        with open(temp_filename, "r") as input_file:
            for line in input_file:
                file_content += line

        expected_content = 'DAY,FEMALE,MALE,SALES\n'
        expected_content += '300.0,200.0,37.572,204.9535\n'
        expected_content += '400.0,0.0,35.57,353.8812\n'
        self.assertEqual(file_content, expected_content)
        os.remove(temp_filename)

        training_filename = 'data/train_invalid_data.csv'  # 1 field has value of 0
        (temp_filename, normalized_list, err) = common_utils.normalize_inputfile(training_filename,
                                                                                 common_utils.NORMALIZE_CONSTRAINT)
        self.assertEqual(err, common_utils.MESSAGE_ERROR_INVALID_DATA_TYPE % training_filename)

        training_filename = 'data/train_normal_classify_label.csv'  # 1 field has value of 0
        (temp_filename, normalized_list, err) = common_utils.normalize_inputfile(training_filename,
                                                                                 common_utils.NORMALIZE_CONSTRAINT,
                                                                                 skip_label_field=True)
        self.assertEqual(err, None)
        file_content = ''
        with open(temp_filename, "r") as input_file:
            for line in input_file:
                file_content += line

        expected_content = 'one2,one,two,three,four,label\n'
        expected_content += '6.4,12.8,2.8,5.6,22.0,label_1\n'
        expected_content += '5.0,10.0,2.3,3.3,10.0,label_2\n'
        self.assertEqual(file_content, expected_content)

    def test_apply_normalize_inputfile(self):

        training_filename = 'data/train_normal_1.csv'  # 1 field has value of 0
        (temp_filename, normalized_list, err) = common_utils.normalize_inputfile(training_filename,
                                                                                 common_utils.NORMALIZE_CONSTRAINT)
        self.assertEqual(err, None)
        file_content = ''
        with open(temp_filename, "r") as input_file:
            for line in input_file:
                file_content += line

        expected_content = 'DAY,FEMALE,MALE,SALES\n'
        expected_content += '3.0,0.62428,3.7572,20.49535\n'
        expected_content += '4.0,0.6443,3.557,35.38812\n'
        self.assertEqual(file_content, expected_content)

        (temp_apply_filename, err) = common_utils.apply_normalize_inputfile(training_filename, normalized_list)

        self.assertEqual(temp_apply_filename, common_utils.create_temp_filename(common_utils.NORMALIZATION_PREFIX, training_filename))

        file_content = ''
        with open(temp_apply_filename, "r") as input_file:
            for line in input_file:
                file_content += line

        self.assertEqual(file_content, expected_content)

        training_filename = 'data/train_normal_classify_label.csv'  # 1 field has value of 0
        (temp_filename, normalized_list, err) = common_utils.normalize_inputfile(training_filename,
                                                                                 common_utils.NORMALIZE_CONSTRAINT,
                                                                                 skip_label_field=True)
        self.assertEqual(err, None)
        file_content = ''
        with open(temp_filename, "r") as input_file:
            for line in input_file:
                file_content += line

        expected_content = 'one2,one,two,three,four,label\n'
        expected_content += '6.4,12.8,2.8,5.6,22.0,label_1\n'
        expected_content += '5.0,10.0,2.3,3.3,10.0,label_2\n'
        self.assertEqual(file_content, expected_content)

        (temp_apply_filename, err) = common_utils.apply_normalize_inputfile(training_filename, normalized_list,
                                                    skip_label_field=True)

        self.assertEqual(temp_apply_filename, common_utils.create_temp_filename(common_utils.NORMALIZATION_PREFIX, training_filename))

        file_content = ''
        with open(temp_apply_filename, "r") as input_file:
            for line in input_file:
                file_content += line

        self.assertEqual(file_content, expected_content)

    def test_verify_column_by_filename(self):
        training_filename = 'data/train.csv'
        predict_filename = 'data/predict.csv'
        test_filename = 'data/test.csv'
        (columns, features, label, err, delimiter_train, delimiter_predict) = \
            common_utils.define_column_feature_label(training_filename, predict_filename)
        self.assertEqual(delimiter_train, ',')
        self.assertEqual(delimiter_predict, None)
        is_equal = common_utils.verify_column_by_filename(columns, test_filename)
        self.assertEqual(is_equal, True)

        is_equal = common_utils.verify_column_by_filename(features, test_filename)
        self.assertEqual(is_equal, False)
        is_equal = common_utils.verify_column_by_filename(None, test_filename)
        self.assertEqual(is_equal, False)


    def test_get_normalize_rate(self):
        # first check int
        reference = 1
        item = 1000
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 100.0)

        reference = 1
        item = 100
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 10.0)

        reference = 1
        item = 1
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 1.0)

        reference = 1000
        item = 1
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 0.01)

        reference = 100
        item = 1
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 0.1)

        reference = 1
        item = 1
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 1.0)

        reference = 35
        item = 43
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 1.0)

        reference = 352
        item = 43
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 1.0)

        reference = 3520
        item = 43
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 0.1)

        reference = 37
        item = 3502
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 10.0)

        # check float

        reference = 1.7
        item = 1000.5
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 100.0)

        reference = 1.7
        item = 120.5343
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 10.0)

        reference = 1.346
        item = 8.392
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 1.0)

        reference = 1000.3902
        item = 2.36
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 0.01)

        reference = 100.20
        item = 1.5
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 0.1)

        reference = 2.8
        item = 1.1
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 1.0)

        # mix int and float
        reference = 2
        item = 1.1
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 1.0)

        reference = 200
        item = 1.1
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 0.01)

        # negative numbers
        reference = -200
        item = 1.1
        result = common_utils.get_normalize_rate(reference, item, 10)
        self.assertEqual(result, 1.0)

    def test_create_temp_filename(self):
        prefix='prefix_'
        filename='/tmp/fjkdjf/tmp.ext'
        temp_filename = common_utils.create_temp_filename(prefix, filename)
        self.assertEqual(temp_filename, '/tmp/fjkdjf/prefix_tmp.ext')

        filename = 'tmp.ext'
        temp_filename = common_utils.create_temp_filename(prefix, filename)
        self.assertEqual(temp_filename, 'prefix_tmp.ext')

        filename = '/tmp'
        temp_filename = common_utils.create_temp_filename(prefix, filename)
        self.assertEqual(temp_filename, '/prefix_tmp')

        filename = 'tmp'
        temp_filename = common_utils.create_temp_filename(prefix, filename)
        self.assertEqual(temp_filename, 'prefix_tmp')

        filename = '/tmp/'
        temp_filename = common_utils.create_temp_filename(prefix, filename)
        self.assertEqual(temp_filename, '/tmp/prefix_')

    # def test_create_meta(self):
    #     model_dir = 'unittest'
    #     columns_list = ['abc', 'def']
    #     train_normalized_list = [1.0, 100.0]
    #
    #     (successful, err) = common_utils.create_meta(model_dir, columns_list, train_normalized_list)
    #     self.assertEqual(err, None)
    #     self.assertEqual(successful, True)
    #     model_meta_dir = common_utils.BASE_MODEL_FOLDER + model_dir + '/' + common_utils.MODEL_METADATA_FOLDER
    #     self.assertEqual(os.path.exists(model_meta_dir), True)
    #
    #     (successful, err) = common_utils.create_meta(model_dir, columns_list, train_normalized_list)
    #     self.assertEqual(successful, False)
    #     self.assertEqual(err, common_utils.MESSAGE_ERROR_MODEL_ALREADY_EXISTS % model_dir)
    #
    #     shutil.rmtree(common_utils.BASE_MODEL_FOLDER)


    def test_create_meta(self):
        model_dir = 'unittest'
        columns_list = ['abc', 'def']
        train_normalized_list = [1.0, 100.0]
        metadata_dict = {}
        metadata_dict[common_utils.METADATA_TAG_COLUMNS] = common_utils.list_to_string(columns_list)
        metadata_dict[common_utils.METADATA_TAG_NORMALIZATION_LIST] = common_utils.list_to_string(train_normalized_list)

        err = common_utils.create_meta(model_dir, metadata_dict)
        self.assertEqual(err, None)
        model_meta_dir = common_utils.BASE_MODEL_FOLDER + model_dir + '/' + common_utils.MODEL_METADATA_FOLDER
        self.assertEqual(os.path.exists(model_meta_dir), True)

        err = common_utils.create_meta(model_dir, metadata_dict)
        self.assertEqual(err, common_utils.MESSAGE_ERROR_MODEL_ALREADY_EXISTS % model_dir)

        shutil.rmtree(common_utils.BASE_MODEL_FOLDER)

    def test_load_metadata_from_model(self):
        model_dir = 'unittest'
        columns_list = ['abc', 'def']
        train_normalized_list = [1.0, 100.0]

        metadata_dict = {}
        metadata_dict[common_utils.METADATA_TAG_COLUMNS] = common_utils.list_to_string(columns_list)
        metadata_dict[common_utils.METADATA_TAG_NORMALIZATION_LIST] = common_utils.list_to_string(train_normalized_list)
        metadata_dict[common_utils.METADATA_TAG_STEPS] = str(common_utils.TRAINING_STEPS)
        metadata_dict[common_utils.METADATA_TAG_HIDDEN_LAYERS] = str(common_utils.HIDDEN_LAYERS)
        metadata_dict[common_utils.METADATA_TAG_DNN_UNITS] = str(common_utils.DNN_UNITS)

        test_dict = {}
        test_set = set()
        test_set.add('m')
        test_set.add('f')
        test_dict['test_set'] = test_set
        metadata_dict['test_key'] = dumps(test_dict, cls=common_utils.PythonObjectEncoder)

        err = common_utils.create_meta(model_dir, metadata_dict)
        self.assertEqual(err, None)

        (metadata_dict, err) = common_utils.load_metadata_from_model(model_dir)
        self.assertEqual(common_utils.TRAINING_STEPS, int(metadata_dict[common_utils.METADATA_TAG_STEPS][0]))
        self.assertEqual(common_utils.HIDDEN_LAYERS, int(metadata_dict[common_utils.METADATA_TAG_HIDDEN_LAYERS][0]))
        self.assertEqual(common_utils.DNN_UNITS, int(metadata_dict[common_utils.METADATA_TAG_DNN_UNITS][0]))
        self.assertEqual(common_utils.DNN_UNITS, int(metadata_dict[common_utils.METADATA_TAG_DNN_UNITS][0]))
        a_dict = loads(metadata_dict['test_key'][0], \
              object_hook=common_utils.as_python_object)

        self.assertEqual(test_dict, a_dict)

        load_columns = metadata_dict[common_utils.METADATA_TAG_COLUMNS]
        load_normalized_list = metadata_dict[common_utils.METADATA_TAG_NORMALIZATION_LIST]
        self.assertEqual(err, None)
        self.assertEqual(load_columns, columns_list)
        self.assertEqual(load_normalized_list, train_normalized_list)

        none_exist_model = 'NoneExistModel'
        (metadata_dict, err) = common_utils.load_metadata_from_model(none_exist_model)
        self.assertEqual(err, common_utils.MESSAGE_ERROR_MODEL_NOT_EXISTS % none_exist_model)
        self.assertEqual(metadata_dict, None)

        shutil.rmtree(common_utils.BASE_MODEL_FOLDER)

    def test_execute_system_call(self):
        command_list = ['ls', '-l']
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(error_code, 0)
        self.assertEqual(error_msg, None)

        command_list = ['/usr/bin/false']
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(result, None)
        self.assertEqual(error_code, 1)

        command_list = ['/none_exist_folder/non_exist_command']
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(result, None)
        self.assertEqual(error_code, 2)
        self.assertEqual(error_msg, 'No such file or directory')


    def test_get_samples_features_count(self):
        filename = 'data/train.csv'
        (sample_count, feature_count, delimiter, label_sets) = common_utils.get_samples_features_count(filename)
        self.assertEqual(sample_count, 2)
        self.assertEqual(feature_count, 3)
        self.assertEqual(delimiter, ',')
        self.assertEqual(len(label_sets), 2)


        filename = 'data/train_no_header_tab.csv'
        (sample_count, feature_count, delimiter, label_sets) = common_utils.get_samples_features_count(filename,
                                                            skip_firstline = False)
        self.assertEqual(sample_count, 2)
        self.assertEqual(feature_count, 3)
        self.assertEqual(delimiter, '\t')
        self.assertEqual(len(label_sets), 2)


        filename = 'data/train_error_1.csv'
        (sample_count, feature_count, delimiter, label_sets) = common_utils.get_samples_features_count(filename)
        self.assertEqual(sample_count, None)
        self.assertEqual(feature_count, None)
        self.assertEqual(delimiter, None)
        self.assertEqual(label_sets, None)


    def test_load_data_to_list_of_list(self):
        filename = 'data/train.csv'
        (data_list, delimiter) = common_utils.load_data_to_list_of_list(filename)
        self.assertEqual(len(data_list), 2)
        self.assertEqual(delimiter, ',')
        data_list_1_str = '3,0.62428,0.37572,20.49535'
        data_list_1 = map(float, data_list_1_str.split(delimiter))
        self.assertEqual(data_list[0], data_list_1)

        data_list_2_str = '4, 0.6443, 0.3557, 35.38812'
        data_list_2 = map(float, data_list_2_str.split(delimiter))
        self.assertEqual(data_list[1], data_list_2)


        filename = 'data/train_no_header_tab.csv'
        (data_list, delimiter) = common_utils.load_data_to_list_of_list(filename, skip_firstline=False)
        self.assertEqual(len(data_list), 2)
        self.assertEqual(delimiter, '\t')
        data_list_1_str = '3\t0.62428\t0.37572\t20.49535'
        data_list_1 = map(float, data_list_1_str.split(delimiter))
        self.assertEqual(data_list[0], data_list_1)

        data_list_2_str = '4\t0.6443\t0.3557\t35.38812'
        data_list_2 = map(float, data_list_2_str.split(delimiter))
        self.assertEqual(data_list[1], data_list_2)


    def test_get_dataset(self):
        filename = 'data/training_dnn_classifier.csv'
        (sample_count, feature_count, delimiter, labels_set) = common_utils.get_samples_features_count(filename)
        self.assertEqual(sample_count, 3)
        self.assertEqual(feature_count, 5)
        self.assertEqual(delimiter, ',')
        self.assertEqual(len(labels_set), 3)
        self.assertEqual('0' in labels_set, True)
        self.assertEqual('1' in labels_set, True)
        self.assertEqual('2' in labels_set, True)

        (data_set, target_set) = common_utils.get_dataset(filename, target_dtype=np.int,
                                                                        features_dtype=np.float32,
                                                                        n_samples=sample_count,
                                                                        n_features=feature_count)

        self.assertEqual(len(target_set), 3)
        self.assertEqual(0 in target_set, True)
        self.assertEqual(1 in target_set, True)
        self.assertEqual(2 in target_set, True)
        self.assertEqual(len(data_set), 3)
        self.assertEqual(len(data_set[0]), 5)
        self.assertEqual(len(data_set[1]), 5)
        self.assertEqual(len(data_set[2]), 5)


        filename = 'data/training_dnn_classifier_scientific_int_float.csv'
        (sample_count, feature_count, delimiter, labels_set) = common_utils.get_samples_features_count(filename)
        self.assertEqual(sample_count, 3)
        self.assertEqual(feature_count, 5)
        self.assertEqual(delimiter, ',')
        self.assertEqual(len(labels_set), 3)
        self.assertEqual('0' in labels_set, True)
        self.assertEqual('1' in labels_set, True)
        self.assertEqual('2' in labels_set, True)

        (data_set, target_set) = common_utils.get_dataset(filename, target_dtype=np.int,
                                                          features_dtype=np.float32,
                                                          n_samples=sample_count,
                                                          n_features=feature_count)

        self.assertEqual(len(target_set), 3)
        self.assertEqual(0 in target_set, True)
        self.assertEqual(1 in target_set, True)
        self.assertEqual(2 in target_set, True)
        self.assertEqual(len(data_set), 3)
        self.assertEqual(len(data_set[0]), 5)
        self.assertEqual(len(data_set[1]), 5)
        self.assertEqual(len(data_set[2]), 5)

        filename = 'data/training_dnn_classifier_tab.csv'
        (sample_count, feature_count, delimiter, labels_set) = common_utils.get_samples_features_count(filename)
        self.assertEqual(sample_count, 3)
        self.assertEqual(feature_count, 5)
        self.assertEqual(delimiter, ',')
        self.assertEqual(len(labels_set), 3)
        self.assertEqual('0' in labels_set, True)
        self.assertEqual('1' in labels_set, True)
        self.assertEqual('2' in labels_set, True)

        (data_set, target_set) = common_utils.get_dataset(filename, target_dtype=np.int,
                                                          features_dtype=np.float32,
                                                          n_samples=sample_count,
                                                          n_features=feature_count)

        self.assertEqual(len(target_set), 3)
        self.assertEqual(0 in target_set, True)
        self.assertEqual(1 in target_set, True)
        self.assertEqual(2 in target_set, True)
        self.assertEqual(len(data_set), 3)
        self.assertEqual(len(data_set[0]), 5)
        self.assertEqual(len(data_set[1]), 5)
        self.assertEqual(len(data_set[2]), 5)

    def test_add_postfix_in_last_occurence_of_string(self):
        in_str = 'abc.def'
        postfix_str = '_postfix'
        occurence_str = '.'
        new_string = common_utils.add_postfix_in_last_occurence_of_string(in_str, postfix_str, occurence_str)
        self.assertEqual(new_string, 'abc_postfix.def')

        occurence_str = '.d'
        new_string = common_utils.add_postfix_in_last_occurence_of_string(in_str, postfix_str, occurence_str)
        self.assertEqual(new_string, 'abc_postfix.def')

        occurence_str = '.def'
        new_string = common_utils.add_postfix_in_last_occurence_of_string(in_str, postfix_str, occurence_str)
        self.assertEqual(new_string, 'abc_postfix.def')

        occurence_str = 'non_exists'
        new_string = common_utils.add_postfix_in_last_occurence_of_string(in_str, postfix_str, occurence_str)
        self.assertEqual(new_string, 'abc.de_postfixf')

    def test_list_to_string(self):
        my_list = ['1', '2', '3']
        my_string = common_utils.list_to_string(my_list)
        self.assertEqual(my_string, '1,2,3')

        my_list = ['1', ['a', 'b', 'c'], '3']
        my_string = common_utils.list_to_string(my_list)
        self.assertEqual(my_string, None)

        my_set = set()
        my_set.add('a')
        my_set.add('b')
        my_list = ['1', my_set, '3']
        my_string = common_utils.list_to_string(my_list)
        self.assertEqual(my_string, None)
        # my_string_list = my_string.split(',')
        # self.assertTrue('1' in my_string_list)
        # self.assertTrue('a' in my_string_list)
        # self.assertTrue('b' in my_string_list)
        # self.assertTrue('3' in my_string_list)

        my_dict = {}
        my_dict['key1']=1
        my_dict['key2']=2
        my_list = ['1', my_dict, '3']
        my_string = common_utils.list_to_string(my_list)
        self.assertEqual(my_string, None)
        # my_string_list = my_string.split(',')
        # self.assertTrue('1' in my_string_list)
        # self.assertTrue('3' in my_string_list)
        # self.assertTrue('key1:1' in my_string)
        # self.assertTrue('key2:2' in my_string)

    # def test_dict_to_string(self):
    #     my_dict={}
    #     my_dict['key1'] = 1
    #     my_dict['key2'] = 2
    #     my_string = common_utils.dict_to_string(my_dict)
    #     self.assertTrue('key1:1' in my_string)
    #     self.assertTrue('key2:2' in my_string)
    #
    #     my_dict={}
    #     my_dict['key1'] = 1
    #     my_dict['key2'] = 2
    #     my_list = ['1', '2', '3']
    #     my_dict['key_list'] = my_list
    #     my_string = common_utils.dict_to_string(my_dict)
    #     self.assertEqual(my_string, None)
    #     # self.assertTrue('key1:1' in my_string)
    #     # self.assertTrue('key2:2' in my_string)
    #     # self.assertTrue('key_list:1,2,3' in my_string)
    #
    #     my_dict={}
    #     my_dict['key1'] = 1
    #     my_dict['key2'] = 2
    #     my_set = set()
    #     my_set.add('a')
    #     my_set.add('b')
    #     my_dict['key_set'] = my_set
    #     my_string = common_utils.dict_to_string(my_dict)
    #     self.assertEqual(my_string, None)
    #     # self.assertTrue('key1:1' in my_string)
    #     # self.assertTrue('key2:2' in my_string)
    #     # self.assertTrue('key_set:a,b' in my_string)
    #
    #     my_dict={}
    #     my_dict['key1'] = 1
    #     my_dict['key2'] = 2
    #     my_dict2 = {}
    #     my_dict2['key21'] = 1
    #     my_dict2['key22'] = 2
    #     my_dict['key_dict'] = my_dict2
    #     my_string = common_utils.dict_to_string(my_dict)
    #     self.assertEqual(my_string, None)
    #     # self.assertTrue('key1:1' in my_string)
    #     # self.assertTrue('key2:2' in my_string)
    #     # self.assertTrue('key_dict:key22:2' in my_string)
    #     # self.assertTrue('key21:1' in my_string)


    def test_map_label_field_to_int(self):
        filename = 'data/training_dnn_classifier_label.csv'
        expected_out_filename = common_utils.add_postfix_in_last_occurence_of_string(filename, common_utils.POSTFIX_RELABEL, '.')
        (out_filename, label_list) = common_utils.map_label_field_to_int(filename)
        self.assertEqual(out_filename, expected_out_filename)
        self.assertEqual(label_list, ['spiecy_1', 'spiecy_2', 'spiecy_3'])

        with open(out_filename, 'r') as in_file:
            line_count = 0
            for line in in_file:
                line = line.strip()
                if line_count == 0:
                    self.assertEqual(line, 'one2,one,two,three,four,five')
                elif line_count == 1:
                    self.assertEqual(line, '6.4,12.8,2.8,5.6,2.2,0')
                elif line_count == 2:
                    self.assertEqual(line, '5,10,2.3,3.3,1,1')
                elif line_count == 3:
                    self.assertEqual(line, '4.9,9.8,3.1,1.5,0.1,2')
                elif line_count == 4:
                    self.assertEqual(line, '5.4,10.8,3.4,1.5,0.4,0')
                elif line_count == 5:
                    self.assertEqual(line, '6.9,13.8,3.1,5.1,2.3,1')
                elif line_count == 6:
                    self.assertEqual(line, '6.7,13.4,3.1,4.4,1.4,2')
                line_count += 1
        os.remove(out_filename)


    def test_apply_map_label_field_to_int(self):
        filename = 'data/training_dnn_classifier_label.csv'
        expected_out_filename = common_utils.add_postfix_in_last_occurence_of_string(filename, common_utils.POSTFIX_RELABEL,
                                                                                     '.')
        (out_filename, label_list) = common_utils.map_label_field_to_int(filename)
        self.assertEqual(out_filename, expected_out_filename)
        self.assertEqual(label_list, ['spiecy_1', 'spiecy_2', 'spiecy_3'])

        testfile = 'data/training_dnn_classifier_test_label.csv'
        expected_out_filename = common_utils.add_postfix_in_last_occurence_of_string(testfile,
                                                                                     common_utils.POSTFIX_RELABEL,
                                                                                     '.')
        (out_filename, _) = common_utils.apply_map_label_field_to_int(testfile, label_list, True)
        self.assertEqual(out_filename, expected_out_filename)
        self.assertEqual(label_list, ['spiecy_1', 'spiecy_2', 'spiecy_3'])

        with open(out_filename, 'r') as in_file:
            line_count = 0
            for line in in_file:
                line = line.strip()
                if line_count == 0:
                    self.assertEqual(line, 'one2,one,two,three,four,five')
                elif line_count == 1:
                    self.assertEqual(line, '4.9,9.8,3.1,1.5,0.1,2')
                elif line_count == 2:
                    self.assertEqual(line, '5,10,2.3,3.3,1,1')
                elif line_count == 3:
                    self.assertEqual(line, '6.4,12.8,2.8,5.6,2.2,0')
                line_count += 1
        os.remove(out_filename)


    def test_remove_last_occurence_of_string(self):
        in_str = 'abc.def'
        occurence_str = 'bc'
        new_string = common_utils.remove_last_occurence_of_string(in_str, occurence_str)
        self.assertEqual(new_string, 'a.def')

        occurence_str = 'a'
        new_string = common_utils.remove_last_occurence_of_string(in_str, occurence_str)
        self.assertEqual(new_string, 'bc.def')

        occurence_str = 'f'
        new_string = common_utils.remove_last_occurence_of_string(in_str, occurence_str)
        self.assertEqual(new_string, 'abc.de')

        occurence_str = ''
        new_string = common_utils.remove_last_occurence_of_string(in_str, occurence_str)
        self.assertEqual(new_string, 'abc.def')

    def test_extract_from_last_occurence_of_string(self):
        in_str = '/abc/def/abc.def'
        occurence_str = '/'
        new_string = common_utils.extract_from_last_occurence_of_string(in_str, occurence_str)
        self.assertEqual(new_string, 'abc.def')

    def test_define_column_feature_label(self):
        training_filename = 'data/train_error_1.csv'
        (columns, features, label, err, delimiter_train, delimiter_predict) = \
            common_utils.define_column_feature_label(training_filename, None)
        self.assertEqual(columns, None)
        self.assertEqual(features, None)
        self.assertEqual(label, None)
        self.assertFalse(err == None)
        self.assertEqual(delimiter_train, None)
        self.assertEqual(delimiter_predict, None)

    def test_is_int_or_float_or_scientific(self):
        item='3'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '3.4'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '.3'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '0.3948394'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '3434'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '0'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '3e5'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '3.4e23'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '.3e1'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '3e0'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '3e-2'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '30e-2'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)
        item = '3.'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), True)

        item = '.3ee'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), False)
        item = '3a0'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), False)
        item = '.3.'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), False)
        item = '..3'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), False)
        item = '30e-2a'
        self.assertEqual(common_utils.is_int_or_float_or_scientific(item), False)

    def test_get_sparse_columns_skip_firstline(self):
        (columns_set, value_set_map, cont_column_set, error) = common_utils.get_sparse_columns_skip_firstline(None)
        self.assertEqual(error, None)
        self.assertEqual(columns_set, None)
        self.assertEqual(value_set_map, None)
        self.assertEqual(cont_column_set, None)

        filename='data/sparse_testfile.csv'
        (columns_set, value_set_map, cont_column_set, error) = common_utils.get_sparse_columns_skip_firstline(filename)
        self.assertEqual(error, None)
        self.assertEqual(len(columns_set), 9)
        self.assertTrue('workclass' in columns_set)
        self.assertTrue('relationship' in columns_set)
        self.assertTrue('gender' in columns_set)
        self.assertTrue('marital_status' in columns_set)
        self.assertTrue('race' in columns_set)
        self.assertTrue('income_bracket' in columns_set)
        self.assertTrue('native_country' in columns_set)
        self.assertTrue('education' in columns_set)
        self.assertTrue('occupation' in columns_set)
        self.assertTrue(len(value_set_map['workclass']), 3)
        self.assertTrue(len(value_set_map['relationship']), 2)
        self.assertTrue(len(value_set_map['race']), 1)
        self.assertTrue(len(value_set_map['occupation']), 3)
        self.assertTrue(len(value_set_map['native_country']), 1)
        self.assertTrue(len(value_set_map['marital_status']), 3)
        self.assertTrue(len(value_set_map['income_bracket']), 1)
        self.assertTrue(len(value_set_map['gender']), 1)
        self.assertTrue(len(value_set_map['education']), 2)
        self.assertEqual(len(cont_column_set), 6)
        self.assertTrue('hours_per_week' in cont_column_set)
        self.assertTrue('age' in cont_column_set)
        self.assertTrue('education_num' in cont_column_set)
        self.assertTrue('capital_loss' in cont_column_set)
        self.assertTrue('fnlwgt' in cont_column_set)
        self.assertTrue('capital_gain' in cont_column_set)

        filename = 'data/sparse_testfile_mismatch.csv'
        (columns_set, value_set_map, cont_column_set, error) = common_utils.get_sparse_columns_skip_firstline(filename)
        self.assertEqual(error, (common_utils.MESSAGE_ERROR_MISMATCHED_COLUMNS_IN_SAME_FILE % \
                                 'sparse_testfile_mismatch.csv'))

if __name__ == '__main__':
    unittest.main()
