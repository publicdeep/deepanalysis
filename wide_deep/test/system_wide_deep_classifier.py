# -*- coding: utf-8 -*-
#import mock
import unittest
import os
import sys
#sys.path.append('../../')
from common import common_utils
import shutil

TEST_STORAGE = '/tmp/storage/unregistered_user/RANDOM_SYSTEM_TEST/'
PATH_TO_DATA = TEST_STORAGE + 'data/'
class TestClass1(unittest.TestCase):
    def setUp(self):
        # copy training file to /tmp/storage/unregistered_user/RANDOM_SYSTEM_TEST/data folder
        os.makedirs(PATH_TO_DATA)
        shutil.copy('../data/train.csv', PATH_TO_DATA)
        shutil.copy('../data/train_deep_only.csv', PATH_TO_DATA)
        shutil.copy('../data/train_wide_only.csv', PATH_TO_DATA)
        shutil.copy('../data/train_scientific.csv', PATH_TO_DATA)
        shutil.copy('../data/train_tab.csv', PATH_TO_DATA)
        shutil.copy('../data/test.csv', PATH_TO_DATA)
        shutil.copy('../data/test_tab.csv', PATH_TO_DATA)
        shutil.copy('../data/test_mismatch.csv', PATH_TO_DATA)
        shutil.copy('../data/test_mismatch_data.csv', PATH_TO_DATA)
        shutil.copy('../data/test_scientific.csv', PATH_TO_DATA)
        shutil.copy('../data/predict.csv', PATH_TO_DATA)
        shutil.copy('../data/predict_tab.csv', PATH_TO_DATA)
        shutil.copy('../data/predict_scientific.csv', PATH_TO_DATA)
        return

    def tearDown(self):
        shutil.rmtree(TEST_STORAGE)
        return

    #@unittest.skip('')
    def test_train_test_predict(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        # with open('/tmp/tmp_results.txt', 'a') as out_file:
        #     out_file.write(result)
        #     out_file.write('\n--------------------\n')
        result = result.strip()
        result_list = result.split('\n')
        accuracy = result_list[0].strip()
        self.assertEqual(accuracy, common_utils.LABEL_TEST_ACCURACY+':')
        val = float(result_list[1].strip())
        #self.assertEqual(accuracy_range1(val) or accuracy_range2(val), True)

        loss = result_list[2].strip()
        self.assertEqual(loss, common_utils.LABEL_TEST_LOSS+':')
        # loss_val varies too much, it's meaningless to validate a huge range
        loss_val = float(result_list[3].strip())
        self.assertTrue(loss_val > 0.0)
        # print('loss: ' + loss_val)

        classification = result_list[4]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':income_bracket')
        classification_result = result_list[5]
        self.assertTrue(len(classification_result) > 0)
        classification_result = result_list[6]
        self.assertTrue(len(classification_result) > 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

    #@unittest.skip('')
    def test_train_only(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=200')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=100')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        result = result.strip()
        self.assertEqual(len(result), 0)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), False)

    #@unittest.skip('')
    def test_train_test(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=200')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=100')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        result = result.strip()
        result_list = result.split('\n')
        accuracy = result_list[0].strip()
        self.assertEqual(accuracy, common_utils.LABEL_TEST_ACCURACY+':')
        val = float(result_list[1].strip())
        #self.assertEqual(accuracy_range(val), True)

        loss = result_list[2].strip()
        self.assertEqual(loss, common_utils.LABEL_TEST_LOSS+':')
        # loss_val varies too much, it's meaningless to validate a huge range
        loss_val = float(result_list[3].strip())
        self.assertTrue(loss_val > 0.0)
        # print('loss: ' + loss_val)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), False)

    #@unittest.skip('')
    def test_train_predict(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=200')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=100')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        result = result.strip()
        result_list = result.split('\n')

        classification = result_list[0]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':income_bracket')
        classification_result = result_list[1]
        self.assertTrue(len(classification_result) > 0)
        classification_result = result_list[2]
        self.assertTrue(len(classification_result) > 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

    #@unittest.skip('')
    def test_load_from_model(self):
        # first train and create model
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(result)
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        result = result.strip()
        result_list = result.split('\n')
        classification = result_list[0]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':income_bracket')
        classification_result = result_list[1]
        self.assertTrue(len(classification_result) > 0)
        classification_result = result_list[2]
        self.assertTrue(len(classification_result) > 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

    #@unittest.skip('')
    def test_load_from_model_with_different_delimiter(self):
        # first train and create model
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(result)
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict_tab.csv')
        command_list.append(common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        result = result.strip()
        result_list = result.split('\n')
        classification = result_list[0]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':income_bracket')
        classification_result = result_list[1]
        self.assertTrue(len(classification_result) > 0)
        classification_result = result_list[2]
        self.assertTrue(len(classification_result) > 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

    #@unittest.skip('')
    def test_train_scientific(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_scientific.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(len(result), 0)
        self.assertEqual(error_code, 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

    #@unittest.skip('')
    def test_train_tab(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_tab.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(len(result), 0)
        self.assertEqual(error_code, 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

    #@unittest.skip('')
    def test_train_deep_only(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_deep_only.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(len(result), 0)
        self.assertEqual(error_code, 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

    #@unittest.skip('')
    def test_train_wide_only(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_wide_only.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(len(result), 0)
        self.assertEqual(error_code, 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)

    #@unittest.skip('')
    def test_train_test_tab_predict(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test_tab.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        # with open('/tmp/tmp_results.txt', 'a') as out_file:
        #     out_file.write(result)
        #     out_file.write('\n--------------------\n')
        result = result.strip()
        result_list = result.split('\n')
        accuracy = result_list[0].strip()
        self.assertEqual(accuracy, common_utils.LABEL_TEST_ACCURACY+':')
        val = float(result_list[1].strip())
        self.assertEqual(accuracy_range1(val) or accuracy_range2(val), True)

        loss = result_list[2].strip()
        self.assertEqual(loss, common_utils.LABEL_TEST_LOSS+':')
        # loss_val varies too much, it's meaningless to validate a huge range
        loss_val = float(result_list[3].strip())
        self.assertTrue(loss_val > 0.0)
        # print('loss: ' + loss_val)

        classification = result_list[4]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':income_bracket')
        classification_result = result_list[5]
        self.assertTrue(len(classification_result) > 0)
        classification_result = result_list[6]
        self.assertTrue(len(classification_result) > 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

    #@unittest.skip('')
    def test_train_test_tab_predict_tab(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_tab.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict_tab.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test_tab.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        #print(result)
        # with open('/tmp/tmp_results.txt', 'a') as out_file:
        #     out_file.write(result)
        #     out_file.write('\n--------------------\n')
        result = result.strip()
        result_list = result.split('\n')
        accuracy = result_list[0].strip()
        self.assertEqual(accuracy, common_utils.LABEL_TEST_ACCURACY+':')
        val = float(result_list[1].strip())
        self.assertEqual(accuracy_range1(val) or accuracy_range2(val), True)

        loss = result_list[2].strip()
        self.assertEqual(loss, common_utils.LABEL_TEST_LOSS+':')
        # loss_val varies too much, it's meaningless to validate a huge range
        loss_val = float(result_list[3].strip())
        self.assertTrue(loss_val > 0.0)
        # print('loss: ' + loss_val)

        classification = result_list[4]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':income_bracket')
        classification_result = result_list[5]
        self.assertTrue(len(classification_result) > 0)
        classification_result = result_list[6]
        self.assertTrue(len(classification_result) > 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)


    #@unittest.skip('')
    def test_train_test_mismatched_field(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test_mismatch.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)

        self.assertEqual(error_code, 2)
        self.assertEqual(error_msg.strip(), common_utils.MESSAGE_ERROR_MISMATCHED_COLUMNS % ('test_mismatch.csv', 'train.csv'))

        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test_mismatch_data.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=50')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        #print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        self.assertEqual(error_code, 1)
        self.assertEqual(error_msg.strip(), \
                         common_utils.MESSAGE_ERROR_MISMATCHED_VALUE_IN_LABEL_COLUMN_WITH_TRAINING_FILE % \
                         ('test_mismatch_data.csv', 'mismatch<=50K'))


    # @unittest.skip('')
    def test_train_test_predict(self):
        model_dir = 'system_model'
        command_list = ['/usr/bin/python', '../wide_deep_classifier.py']
        command_list.append(common_utils.CMD_OPT_TRAIN_FILENAME + '=' + PATH_TO_DATA + 'train_scientific.csv')
        command_list.append(common_utils.CMD_OPT_PREDICT_FILENAME + '=' + PATH_TO_DATA + 'predict_scientific.csv')
        command_list.append(common_utils.CMD_OPT_TEST_FILENAME + '=' + PATH_TO_DATA + 'test_scientific.csv')
        command_list.append(common_utils.CMD_OPT_OUTPUT_DIR + '=' + TEST_STORAGE)
        command_list.append(common_utils.CMD_OPT_MODEL_DIR_STR + '=' + model_dir)
        command_list.append(common_utils.CMD_OPT_STEPS + '=100')
        command_list.append(common_utils.CMD_OPT_DNN_UNITS + '=5')
        command_list.append(common_utils.CMD_OPT_DISABLE_NORMALIZATION)

        # print(str(command_list))
        (result, error_code, error_msg) = common_utils.execute_system_call(command_list)
        # print(result)
        # with open('/tmp/tmp_results.txt', 'a') as out_file:
        #     out_file.write(result)
        #     out_file.write('\n--------------------\n')
        result = result.strip()
        result_list = result.split('\n')
        accuracy = result_list[0].strip()
        self.assertEqual(accuracy, common_utils.LABEL_TEST_ACCURACY + ':')
        val = float(result_list[1].strip())
        # self.assertEqual(accuracy_range1(val) or accuracy_range2(val), True)

        loss = result_list[2].strip()
        self.assertEqual(loss, common_utils.LABEL_TEST_LOSS + ':')
        # loss_val varies too much, it's meaningless to validate a huge range
        loss_val = float(result_list[3].strip())
        self.assertTrue(loss_val > 0.0)
        # print('loss: ' + loss_val)

        classification = result_list[4]
        self.assertEqual(classification, common_utils.LABEL_CLASSIFICATION + ':income_bracket')
        classification_result = result_list[5]
        self.assertTrue(len(classification_result) > 0)
        classification_result = result_list[6]
        self.assertTrue(len(classification_result) > 0)

        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.BASE_MODEL_FOLDER + model_dir), True)
        self.assertEqual(os.path.exists(TEST_STORAGE + common_utils.OUTPUT_FILENAME), True)

def accuracy_range1(val):
    return val > 0.75 and val < 0.82
def accuracy_range2(val):
    return val > 0.23 and val < 0.29

if __name__ == '__main__':
    unittest.main()
