# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Example code for TensorFlow Wide & Deep Tutorial using TF.Learn API."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tempfile
#from six.moves import urllib

import pandas as pd
import tensorflow as tf
from common import common_utils
import itertools
import numpy as np

flags = tf.app.flags
FLAGS = flags.FLAGS

flags.DEFINE_string("model_dir", "", "Base directory for output models.")
flags.DEFINE_string("model_type", "wide_n_deep",
                    "Valid model types: {'wide', 'deep', 'wide_n_deep'}.")
flags.DEFINE_integer("train_steps", 200, "Number of training steps.")
flags.DEFINE_string(
    "train_data",
    "",
    "Path to the training data.")
flags.DEFINE_string(
    "test_data",
    "",
    "Path to the test data.")

# COLUMNS = ["age", "workclass", "fnlwgt", "education", "education_num",
#            "marital_status", "occupation", "relationship", "race", "gender",
#            "capital_gain", "capital_loss", "hours_per_week", "native_country",
#            "income_bracket"]
#LABEL_COLUMN = None
# CATEGORICAL_COLUMNS = ["workclass", "education", "marital_status", "occupation",
#                        "relationship", "race", "gender", "native_country"]
# CONTINUOUS_COLUMNS = ["age", "education_num", "capital_gain", "capital_loss",
#                       "hours_per_week"]


def maybe_download():
  """Maybe downloads training data and returns train and test file names."""
  # if FLAGS.train_data:
  #   train_file_name = FLAGS.train_data
  # else:
  #   train_file = tempfile.NamedTemporaryFile(delete=False)
  #   urllib.request.urlretrieve("https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data", train_file.name)  # pylint: disable=line-too-long
  #   train_file_name = train_file.name
  #   train_file.close()
  #   print("Training data is downloaded to %s" % train_file_name)
  #
  # if FLAGS.test_data:
  #   test_file_name = FLAGS.test_data
  # else:
  #   test_file = tempfile.NamedTemporaryFile(delete=False)
  #   urllib.request.urlretrieve("https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.test", test_file.name)  # pylint: disable=line-too-long
  #   test_file_name = test_file.name
  #   test_file.close()
  #   print("Test data is downloaded to %s" % test_file_name)

  train_file_name = 'data/train.csv'
  test_file_name = 'data/test.csv'
  return train_file_name, test_file_name


def build_estimator(model_dir, sparse_column_value_map, real_val_column):
  """Build an estimator."""
  # Sparse base columns.

  wide_columns = []
  for item in sparse_column_value_map.keys():
      keys = []
      for key in sparse_column_value_map[item]:
          keys.append(key)
      tmp_column = tf.contrib.layers.sparse_column_with_keys(column_name=item,
                                                     keys=keys)
      wide_columns.append(tmp_column)

  # Continuous base columns.
  deep_columns = []
  for item in real_val_column:
      deep_columns.append(tf.contrib.layers.real_valued_column(item))

  # Transformations.
  # age_buckets = tf.contrib.layers.bucketized_column(age,
  #                                                   boundaries=[
  #                                                       18, 25, 30, 35, 40, 45,
  #                                                       50, 55, 60, 65
  #                                                   ])
  #
  # Wide columns and deep columns.
  # wide_columns = [gender, native_country, education, occupation, workclass,
  #                 relationship, age_buckets,
  #                 tf.contrib.layers.crossed_column([education, occupation],
  #                                                  hash_bucket_size=int(1e4)),
  #                 tf.contrib.layers.crossed_column(
  #                     [age_buckets, education, occupation],
  #                     hash_bucket_size=int(1e6)),
  #                 tf.contrib.layers.crossed_column([native_country, occupation],
  #                                                  hash_bucket_size=int(1e4))]

  deep_columns2 = []
  for item in wide_columns:
      deep_columns2.append(tf.contrib.layers.embedding_column(item, dimension=8))

  deep_columns = deep_columns + deep_columns2


  if FLAGS.model_type == "wide":
    m = tf.contrib.learn.LinearClassifier(model_dir=model_dir,
                                          feature_columns=wide_columns)
  elif FLAGS.model_type == "deep":
    m = tf.contrib.learn.DNNClassifier(model_dir=model_dir,
                                       feature_columns=deep_columns,
                                       hidden_units=[100, 100])
  else:
    m = tf.contrib.learn.DNNLinearCombinedClassifier(
        model_dir=model_dir,
        linear_feature_columns=wide_columns,
        dnn_feature_columns=deep_columns,
        dnn_hidden_units=[100, 100])
  return m


def input_fn(df, label_column, sparse_column, cont_column, is_predict = False):
  """Input builder function."""
  # Creates a dictionary mapping from each continuous feature column name (k) to
  # the values of that column stored in a constant Tensor.
  continuous_cols = {k: tf.constant(df[k].values) for k in cont_column}
  # Creates a dictionary mapping from each categorical feature column name (k)
  # to the values of that column stored in a tf.SparseTensor.
  categorical_cols = {k: tf.SparseTensor(
      indices=[[i, 0] for i in range(df[k].size)],
      values=df[k].values,
      shape=[df[k].size, 1])
                      for k in sparse_column}
  # Merges the two dictionaries into one.
  feature_cols = dict(continuous_cols)
  feature_cols.update(categorical_cols)
  # Converts the label column into a constant Tensor.
  if (is_predict == True):
    return feature_cols

  label = tf.constant(df[label_column].values)
  # Returns the feature columns and the label.
  return feature_cols, label


def train_and_eval():
  """Train and evaluate the model."""
  train_file_name, test_file_name = maybe_download()
  disable_normalization = True
  (temp_train_filename, metadata_label_list) = common_utils.map_label_field_to_int(train_file_name)
  (temp_test_filename, error_msg) = common_utils.apply_map_label_field_to_int(test_file_name, \
                                                                              metadata_label_list,
                                                                              disable_normalization)

  if (error_msg != None):
      common_utils.exit_with_error(error_msg, 1)

  (COLUMNS, features, LABEL_COLUMN, err, common_utils.DELIMITER_INPUT_TRAIN, common_utils.DELIMITER_INPUT_PREDICT) = \
      common_utils.define_column_feature_label(train_file_name, None)
  if (err != None):
      common_utils.exit_with_error(err)

  df_train = pd.read_csv(
      tf.gfile.Open(temp_train_filename),
      names=COLUMNS,
      skipinitialspace=True,
      skiprows=1,
      engine="python")
  df_test = pd.read_csv(
      tf.gfile.Open(temp_test_filename),
      names=COLUMNS,
      skipinitialspace=True,
      skiprows=1,
      engine="python")

  # remove NaN elements
  df_train = df_train.dropna(how='any', axis=0)
  df_test = df_test.dropna(how='any', axis=0)

  # df_train[LABEL_COLUMN] = (
  #     df_train[LABEL_COLUMN].apply(lambda x: ">50K" in x)).astype(int)
  # df_test[LABEL_COLUMN] = (
  #     df_test[LABEL_COLUMN].apply(lambda x: ">50K" in x)).astype(int)

  #model_dir = tempfile.mkdtemp() if not FLAGS.model_dir else FLAGS.model_dir
  model_dir='model'
  print("model directory = %s" % model_dir)

  (sparse_column, sparse_column_value_map, cont_column, error) = \
      common_utils.get_sparse_columns_skip_firstline(train_file_name)

  real_val_column = cont_column.copy()
  try:
    real_val_column.remove(LABEL_COLUMN)
  except KeyError:
    pass

  try:
    del sparse_column_value_map[LABEL_COLUMN]
  except KeyError:
    pass

  m = build_estimator(model_dir, sparse_column_value_map, real_val_column)
  m.fit(input_fn=lambda: input_fn(df_train, LABEL_COLUMN, sparse_column, cont_column), steps=FLAGS.train_steps)
  results = m.evaluate(input_fn=lambda: input_fn(df_test, LABEL_COLUMN, sparse_column, cont_column), steps=1)
  for key in sorted(results):
    print("%s: %s" % (key, results[key]))

  df_predict = pd.read_csv(
      tf.gfile.Open('data/predict.csv'),
      names=features,
      skipinitialspace=True,
      skiprows=1,
      engine="python")

  try:
    sparse_column.remove(LABEL_COLUMN)
  except KeyError:
    pass

  try:
    cont_column.remove(LABEL_COLUMN)
  except KeyError:
    pass

  y = m.predict(input_fn=lambda: input_fn(df_predict, LABEL_COLUMN, sparse_column, cont_column, True))

  y2 = list(itertools.islice(y, df_predict.shape[0]))
  for item in y2:
    print(metadata_label_list[item])

  # save to file and output
  # with open(common_utils.OUTPUT_DIR + common_utils.OUTPUT_FILENAME, 'w') as output_file:
  #   print(common_utils.LABEL_CLASSIFICATION + ':' + str(label))
  #     for item in y2:
  #       print(metadata_label_list[item])


def main(_):
  train_and_eval()


if __name__ == "__main__":
  tf.app.run()
