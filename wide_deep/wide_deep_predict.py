# -*- coding: utf-8 -*-
#  Copyright 2017 All Rights Reserved.
#
"""DNN Classifier with custom input_fn.
"""
# python wide_deep_predict.py --train-filename=data/train_wdp.csv --predict-filename=data/predict_wdp.csv --test-filename=data/test_wdp.csv --output-dir=./ --model_dir=predict_model --steps=5000 --dnn-units=2 --disable-normal

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pandas as pd
import tensorflow as tf
import sys
import itertools
import numpy as np

# from dict to json: http://stackoverflow.com/questions/8230315/python-sets-are-not-json-serializable
from json import dumps, loads, JSONEncoder, JSONDecoder
import pickle

# class PythonObjectEncoder(JSONEncoder):
#     def default(self, obj):
#         if isinstance(obj, (list, dict, str, unicode, int, float, bool, type(None))):
#             return JSONEncoder.default(self, obj)
#         return {'_python_object': pickle.dumps(obj)}
#
# def as_python_object(dct):
#     if '_python_object' in dct:
#         return pickle.loads(str(dct['_python_object']))
#     return dct

from common import common_utils

HELP_STRING = 'python wide_deep_classifier.py --train-filename=[train.csv] --predict-filename=[predict.csv]\n \
    <--test-filename=[test.csv]> <--enable-logging> <--enable-normal> <--model-dir=[/tmp/model_dir]> \n \
    <--load-model-dir=[/tmp/model_dir]> <--steps=[5000]> <--hidden-layers=[2]> <--dnn-units=[10]> \n \
    --output-dir=[/tmp/storage/]'

def input_fn(df, label_column, sparse_column, cont_column, is_predict = False):
    """Input builder function."""
    # Creates a dictionary mapping from each continuous feature column name (k) to
    # the values of that column stored in a constant Tensor.
    continuous_cols = {k: tf.constant(df[k].values) for k in cont_column}
    # Creates a dictionary mapping from each categorical feature column name (k)
    # to the values of that column stored in a tf.SparseTensor.
    categorical_cols = {k: tf.SparseTensor(
      indices=[[i, 0] for i in range(df[k].size)],
      values=df[k].values,
      shape=[df[k].size, 1])
                      for k in sparse_column}
    # Merges the two dictionaries into one.
    feature_cols = dict(continuous_cols)
    feature_cols.update(categorical_cols)
    # Converts the label column into a constant Tensor.
    if (is_predict == True):
        return feature_cols

    label = tf.constant(df[label_column].values)
    # Returns the feature columns and the label.
    return feature_cols, label

def build_estimator(model_dir, sparse_column_value_map, real_val_column, hidden_layers, training_mode):
    """Build an estimator."""
    # Sparse base columns.
    wide_columns = []
    for item in sparse_column_value_map.keys():
        keys = []
        for key in sparse_column_value_map[item]:
            keys.append(key)
        tmp_column = tf.contrib.layers.sparse_column_with_keys(column_name=item,
                                                     keys=keys)
        wide_columns.append(tmp_column)

    # Continuous base columns.
    deep_columns = []
    for item in real_val_column:
        deep_columns.append(tf.contrib.layers.real_valued_column(item))

    # Transformations.
    # age_buckets = tf.contrib.layers.bucketized_column(age,
    #                                                   boundaries=[
    #                                                       18, 25, 30, 35, 40, 45,
    #                                                       50, 55, 60, 65
    #                                                   ])
    #
    # Wide columns and deep columns.
    # wide_columns = [gender, native_country, education, occupation, workclass,
    #                 relationship, age_buckets,
    #                 tf.contrib.layers.crossed_column([education, occupation],
    #                                                  hash_bucket_size=int(1e4)),
    #                 tf.contrib.layers.crossed_column(
    #                     [age_buckets, education, occupation],
    #                     hash_bucket_size=int(1e6)),
    #                 tf.contrib.layers.crossed_column([native_country, occupation],
    #                                                  hash_bucket_size=int(1e4))]

    deep_columns2 = []
    for item in wide_columns:
        deep_columns2.append(tf.contrib.layers.embedding_column(item, dimension=1))

    deep_columns = deep_columns + deep_columns2


    if training_mode == common_utils.TRAINING_MODE_WIDE:
        m = tf.contrib.learn.LinearClassifier(model_dir=model_dir,
                                              feature_columns=wide_columns)
    elif training_mode == common_utils.TRAINING_MODE_DEEP:
        m = tf.contrib.learn.DNNClassifier(model_dir=model_dir,
                                           feature_columns=deep_columns,
                                           hidden_units=hidden_layers)
    else:
        # m = tf.contrib.learn.DNNLinearCombinedClassifier(
        #     model_dir=model_dir,
        #     linear_feature_columns=wide_columns,
        #     dnn_feature_columns=deep_columns,
        #     dnn_hidden_units=hidden_layers)
        m = tf.contrib.learn.DNNRegressor(
            feature_columns=deep_columns,
            hidden_units=hidden_layers,
            model_dir=model_dir)
    return m

def main(argv):

    enable_logging = False
    model_dir = None
    load_from_model_dir = None
    train_filename = None
    test_filename = None
    predict_filename = None
    disable_normalization = False

    for arg_item in argv:
        if (arg_item == common_utils.CMD_OPT_ENABLE_LOGGING_STR):
            enable_logging = True
        elif (arg_item.startswith(common_utils.CMD_OPT_MODEL_DIR_STR)):
            model_dir = arg_item.split(common_utils.DELIMITER_EQUAL)[1]
        elif (arg_item.startswith(common_utils.CMD_OPT_LOAD_FROM_MODEL_DIR_STR)):
            load_from_model_dir = arg_item.split(common_utils.DELIMITER_EQUAL)[1]
        elif (arg_item.startswith(common_utils.CMD_OPT_TRAIN_FILENAME)):
            train_filename = arg_item.split(common_utils.DELIMITER_EQUAL)[1]
        elif (arg_item.startswith(common_utils.CMD_OPT_TEST_FILENAME)):
            test_filename = arg_item.split(common_utils.DELIMITER_EQUAL)[1]
        elif (arg_item.startswith(common_utils.CMD_OPT_PREDICT_FILENAME)):
            predict_filename = arg_item.split(common_utils.DELIMITER_EQUAL)[1]
        elif (arg_item == common_utils.CMD_OPT_DISABLE_NORMALIZATION):
            disable_normalization = True
        elif (arg_item.startswith(common_utils.CMD_OPT_STEPS)):
            common_utils.TRAINING_STEPS = min(int(arg_item.split(common_utils.DELIMITER_EQUAL)[1]), common_utils.MAX_TRAINING_STEPS)
        elif (arg_item.startswith(common_utils.CMD_OPT_HIDDEN_LAYERS)):
            common_utils.HIDDEN_LAYERS = min(int(arg_item.split(common_utils.DELIMITER_EQUAL)[1]), common_utils.MAX_HIDDEN_LAYERS)
        elif (arg_item.startswith(common_utils.CMD_OPT_DNN_UNITS)):
            common_utils.DNN_UNITS = min(int(arg_item.split(common_utils.DELIMITER_EQUAL)[1]), common_utils.MAX_DNN_UNITS)
        elif (arg_item.startswith(common_utils.CMD_OPT_OUTPUT_DIR)):
            common_utils.OUTPUT_DIR = arg_item.split(common_utils.DELIMITER_EQUAL)[1]

    if common_utils.OUTPUT_DIR == None:
        common_utils.exit_with_error(HELP_STRING)
    if train_filename == None:
        if predict_filename == None:
            common_utils.exit_with_error(HELP_STRING)
        else:
            if load_from_model_dir == None:
                common_utils.exit_with_error(HELP_STRING)

    columns = None
    features = None
    label = None
    if train_filename != None:
        (columns, features, label, err, common_utils.DELIMITER_INPUT_TRAIN, common_utils.DELIMITER_INPUT_PREDICT) = \
            common_utils.define_column_feature_label(train_filename, predict_filename)
        if (err != None):
            common_utils.exit_with_error(err)

    if test_filename != None and common_utils.verify_column_by_filename(columns, test_filename) == False:
        common_utils.exit_with_error(common_utils.MESSAGE_ERROR_MISMATCHED_COLUMNS % \
                                     (common_utils.extract_from_last_occurence_of_string(test_filename, '/'), \
                                      common_utils.extract_from_last_occurence_of_string(train_filename, '/')), \
                                     common_utils.MESSAGE_ERROR_MISMATCHED_COLUMNS_CODE)
    if enable_logging == True:
        tf.logging.set_verbosity(tf.logging.INFO)

    train_normalized_list = []

    temp_train_filename = train_filename
    temp_predict_filename = predict_filename
    temp_test_filename = test_filename

    # check if it's int and float only.  Otherwise, still need to disable normalization for now
    if (temp_train_filename != None):
        (valid, err, common_utils.DELIMITER_INPUT_TRAIN) = common_utils.verify_int_float_data_by_filename(temp_train_filename, skip_label_field=True)
        if valid == False:
            disable_normalization = True

    if (temp_predict_filename != None):
        (valid, err, common_utils.DELIMITER_INPUT_PREDICT) = common_utils.verify_int_float_data_by_filename(temp_predict_filename)
        if valid == False:
            disable_normalization = True

    if (temp_test_filename != None):
        (valid, err, common_utils.DELIMITER_INPUT_TEST) = common_utils.verify_int_float_data_by_filename(temp_test_filename, skip_label_field=True)
        if valid == False:
            disable_normalization = True

    if disable_normalization == False and train_filename != None:
        (temp_train_filename, train_normalized_list, err) = common_utils.normalize_inputfile(train_filename, common_utils.NORMALIZE_CONSTRAINT, skip_label_field=True)
        if err != None:
            common_utils.exit_with_error(err)

        (temp_predict_filename, err) = common_utils.apply_normalize_inputfile(predict_filename, train_normalized_list)
        if err != None:
            common_utils.exit_with_error(err)

        (temp_test_filename, err) = common_utils.apply_normalize_inputfile(test_filename, train_normalized_list, skip_label_field = True)
        if err != None:
            common_utils.exit_with_error(err)
    elif train_filename != None:
        pass

    # Load datasets
    metadata_label_list = []
    training_feature_count = None
    sparse_column = None
    sparse_column_value_map = {}
    cont_column = None
    if (temp_train_filename != None):
        (sparse_column, sparse_column_value_map, cont_column, error) = \
            common_utils.get_sparse_columns_skip_firstline(temp_train_filename)
        #(temp_train_filename, metadata_label_list) = common_utils.map_label_field_to_int(temp_train_filename)

        training_set_data = pd.read_csv(
            tf.gfile.Open(temp_train_filename),
            names=columns,
            skipinitialspace=True,
            skiprows=1,
            delimiter=common_utils.DELIMITER_INPUT_TRAIN,
            engine="python")
        training_set_data = training_set_data.dropna(how='any', axis=0)


        # (training_sample_count, training_feature_count, _, label_sets) = common_utils.get_samples_features_count(temp_train_filename)
        #
        # (training_set_data, training_set_target) = common_utils.get_dataset(temp_train_filename, target_dtype=np.int, \
        #                                                                 features_dtype=np.float32,
        #                                                                 n_samples=training_sample_count,
        #                                                                 n_features=training_feature_count)

    if load_from_model_dir != None:
        #(columns, train_normalized_list, err) = common_utils.load_metadata_from_model(load_from_model_dir)
        (metadata_dict, err) = common_utils.load_metadata_from_model(load_from_model_dir)
        common_utils.TRAINING_STEPS = int(metadata_dict[common_utils.METADATA_TAG_STEPS][0])
        common_utils.HIDDEN_LAYERS = int(metadata_dict[common_utils.METADATA_TAG_HIDDEN_LAYERS][0])
        common_utils.DNN_UNITS = int(metadata_dict[common_utils.METADATA_TAG_DNN_UNITS][0])
        columns = metadata_dict[common_utils.METADATA_TAG_COLUMNS]
        sparse_column = set(metadata_dict[common_utils.METADATA_TAG_SPARSE_COLUMNS])
        cont_column = set(metadata_dict[common_utils.METADATA_TAG_CONTINUOUS_COLUMNS])
        for item in metadata_dict[common_utils.METADATA_TAG_SPARSE_VALUES_DICT]:
            # when dump the dict to json string, there is an extra pair of {}, should remove it
            if item.startswith('{'):
                item = item[1:len(item)]
            elif item.endswith('}}'):
                item = item[:len(item)-1]
            item_list = item.split(':', 1)
            sparse_column_value_map[item_list[0].strip().strip('"')] = loads(item_list[1], \
                                                        object_hook=common_utils.as_python_object)


        metadata_label_list = metadata_dict[common_utils.METADATA_TAG_LABEL_LIST]
        training_feature_count = len(columns) - 1

        if metadata_dict.has_key(common_utils.METADATA_TAG_NORMALIZATION_LIST):
            train_normalized_list = metadata_dict[common_utils.METADATA_TAG_NORMALIZATION_LIST]
        if err != None:
            common_utils.exit_with_error(err)
        label = columns[len(columns) - 1]
        (temp_predict_filename, err) = common_utils.apply_normalize_inputfile(temp_predict_filename, train_normalized_list)
        if err != None:
            common_utils.exit_with_error(err)

    # Feature cols
    #feature_cols = [tf.contrib.layers.real_valued_column("", dimension=training_feature_count)]

    # Build hidden layers fully connected DNN with units.
    hidden_layers = []
    for layer_no in range(common_utils.HIDDEN_LAYERS):
      hidden_layers.append(common_utils.DNN_UNITS)

    wide_deep_model = None
    real_val_column = cont_column.copy()
    try:
        real_val_column.remove(label)
    except KeyError:
        pass

    if load_from_model_dir == None:
        try:
            if model_dir != None and common_utils.check_model_exists(model_dir) == True:
                common_utils.exit_with_error(common_utils.MESSAGE_ERROR_MODEL_ALREADY_EXISTS % model_dir)
            model_result_dir = common_utils.get_model_result_dirname(model_dir)

            try:
                del sparse_column_value_map[label]
            except KeyError:
                pass

            # Build layered DNN with units respectively.
            wide_deep_model = build_estimator(model_result_dir, sparse_column_value_map, real_val_column, hidden_layers, None)
            wide_deep_model.fit(input_fn=lambda: input_fn(training_set_data, label, sparse_column, cont_column), \
                  steps=common_utils.TRAINING_STEPS)


        except tf.python.framework.errors.UnimplementedError as e:
            common_utils.exit_with_error(e.message)
        except:
            error = sys.exc_info()
            common_utils.exit_with_error(error[1].message)

    else:

        final_load_from_model_dir = ''
        if common_utils.OUTPUT_DIR != None:
            final_load_from_model_dir = common_utils.OUTPUT_DIR
        final_load_from_model_dir += common_utils.BASE_MODEL_FOLDER + load_from_model_dir
        final_load_from_model_dir += common_utils.DELIMITER_DIRECTORY + common_utils.MODEL_RESULT_FOLDER
        wide_deep_model = build_estimator(final_load_from_model_dir, sparse_column_value_map, real_val_column, \
                                          hidden_layers, None)
        # Build layered DNN with units respectively.
        # classifier = tf.contrib.learn.DNNClassifier(feature_columns=feature_cols,
        #                                             hidden_units=hidden_layers,
        #                                             n_classes=len(metadata_label_list),
        #                                             model_dir=final_load_from_model_dir)


    # Score accuracy
    accuracy_score = None
    if temp_test_filename != None:

        # (temp_test_filename, error_msg) = common_utils.apply_map_label_field_to_int(temp_test_filename, \
        #                                                                             metadata_label_list,
        #                                                                             disable_normalization)
        # if error_msg != None:
        #     common_utils.exit_with_error(error_msg, 1)

        df_test = pd.read_csv(
            tf.gfile.Open(temp_test_filename),
            names=columns,
            skipinitialspace=True,
            skiprows=1,
            delimiter=common_utils.DELIMITER_INPUT_TEST,
            engine="python")

        results = wide_deep_model.evaluate(input_fn=lambda: input_fn(df_test, label, sparse_column, cont_column), steps=1)
        output_key_list = [common_utils.LABEL_TEST_ACCURACY, common_utils.LABEL_TEST_LOSS]
        # for key in output_key_list:
        #     print(key + ':')
        #     print (results[key])

    if load_from_model_dir == None and model_dir != None:
            # save columns and train_normalized_list
            metadata_dict = {}
            metadata_dict[common_utils.METADATA_TAG_COLUMNS] = common_utils.list_to_string(columns)
            metadata_dict[common_utils.METADATA_TAG_SPARSE_COLUMNS] = common_utils.list_to_string(sparse_column)
            metadata_dict[common_utils.METADATA_TAG_CONTINUOUS_COLUMNS] = \
                common_utils.list_to_string(cont_column)

            metadata_dict[common_utils.METADATA_TAG_NORMALIZATION_LIST] = \
                common_utils.list_to_string(train_normalized_list)
            metadata_dict[common_utils.METADATA_TAG_LABEL_LIST] = \
                common_utils.list_to_string(metadata_label_list)
            metadata_dict[common_utils.METADATA_TAG_STEPS] = str(common_utils.TRAINING_STEPS)
            metadata_dict[common_utils.METADATA_TAG_HIDDEN_LAYERS] = str(common_utils.HIDDEN_LAYERS)
            metadata_dict[common_utils.METADATA_TAG_DNN_UNITS] = str(common_utils.DNN_UNITS)
            metadata_dict[common_utils.METADATA_TAG_SPARSE_VALUES_DICT] = \
                dumps(sparse_column_value_map, cls=common_utils.PythonObjectEncoder)

            if accuracy_score != None:
                metadata_dict[common_utils.METADATA_TAG_DNN_CLASSIFICATION_ACCURACY] = str(accuracy_score)
            err = common_utils.create_meta(model_dir, metadata_dict)
            if err != None:
                common_utils.exit_with_error(err)

    # Print out predictions
    if (temp_predict_filename is not None):
        if features==None:
            features=columns[:]
            try:
                features.remove(label)
            except KeyError:
                pass
        df_predict = pd.read_csv(
            tf.gfile.Open(temp_predict_filename),
            names=features,
            skipinitialspace=True,
            skiprows=1,
            delimiter=common_utils.DELIMITER_INPUT_PREDICT,
            engine="python")
        try:
            sparse_column.remove(label)
        except KeyError:
            pass

        try:
            cont_column.remove(label)
        except KeyError:
            pass

        y = wide_deep_model.predict(input_fn=lambda: input_fn(df_predict, label, sparse_column, cont_column, True))
        y2 = list(itertools.islice(y, df_predict.shape[0]))

        # save to file and output
        with open(common_utils.OUTPUT_DIR + common_utils.OUTPUT_FILENAME, 'w') as output_file:
            print(common_utils.LABEL_CLASSIFICATION + ':'+str(label))
            for item in y2:
                #print(metadata_label_list[item])
                print (item)

                # output_file.write(str(metadata_label_list[item]))
                # output_file.write('\n')


if __name__ == "__main__":
    tf.app.run()
    sys.exit(0)
