test:
	cd common/test; python test_common_utils.py

	cd prototype/test; python system_integration_test.py

	cd imagenet/test; python system_integration_test.py

	cd dnn_classifer/test; python system_dnn_classifier.py

	cd wide_deep/test; python system_wide_deep_classifier.py

